﻿using FactorioDataReader;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_WPFDataReader.ViewModel
{
    public class GameDataVM
    {
        public ItemTableVM ItemTableVm { get; private set; }
        public RecipeTableVM RecipeTableVm { get; private set; }

        public GameDataVM()
        {
            GameData gameData = new GameData();
            gameData.Difficulty = Difficulty.Normal;
            gameData.EnabledMods = null;
            gameData.Locale = "en";

            ItemTableVm = ItemTableVM.LoadItemCollection(gameData.Items);
            RecipeTableVm =  RecipeTableVM.LoadRecipeCollection(gameData.Recipes);
        }
    }

}
