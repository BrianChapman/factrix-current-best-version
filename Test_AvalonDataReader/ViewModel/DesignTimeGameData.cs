﻿using FactorioDataReader;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_WPFDataReader.ViewModel
{
    public class DesignTimeGameDataVM : INotifyPropertyChanged
    {
        //public ItemTableVM ItemTableVm { get; private set; }
        //public RecipeTableVM RecipeTableVm { get; private set; }

        //string[,] itemData = new string[2, 4] {
        //    { "iron-plate", "Item", @"Assets/iron-plate.png", "Iron Plate" },
        //    { "iron-ore", "Item", @"Assets/iron-ore.png", "Iron Ore" },
        //};
        //public DesignTimeGameDataVM()
        //{
        //    var itemTable = new ItemTable();
        //    for (int i = 0; i < 2; ++i)
        //    {
        //        var ironItem = new Item(itemData[i, 0], itemData[i, 1], itemData[i, 2]);
        //        ironItem.FriendlyName = itemData[i, 3];
        //        itemTable.Add(itemData[i, 0], ironItem);
        //    }
        //    ItemTableVm = ItemTableVM.LoadItemCollection(itemTable);

        //    var recipeTable = new RecipeTable();

        //    var ingredients = new ItemResourceTable();
        //    var ingred = new ItemResource(itemData[1, 0], 1);
        //    ingredients.Add(ingred.ItemName, ingred);

        //    var results = new ItemResourceTable();
        //    var result = new ItemResource(itemData[0, 0], 1);
        //    results.Add(result.ItemName, result);

        //    var ironRecipe = new Recipe(itemData[0, 0], ingredients, results);
        //    ironRecipe.FriendlyName = itemData[0, 3];
        //    recipeTable.Add(ironRecipe.Name, ironRecipe);
        //    RecipeTableVm = RecipeTableVM.LoadRecipeCollection(recipeTable);
        //}


        public event PropertyChangedEventHandler PropertyChanged;

        void Notify(string prop)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(prop));
        }

    }
}
