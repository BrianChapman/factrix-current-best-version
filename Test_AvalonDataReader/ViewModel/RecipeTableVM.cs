﻿using FactorioDataReader;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_WPFDataReader.ViewModel
{
    public class RecipeTableVM: ObservableCollection<Recipe>
    {

        class RecipeSorter : Comparer<Recipe>
        {
            public override int Compare(Recipe a, Recipe b)
            {
                return string.Compare(a.Name, b.Name);
            }
        }


        Recipe _selectedRecipe;
        public Recipe SelectedRecipe
        {
            get { return _selectedRecipe; }
            set
            {
                _selectedRecipe = value;
                OnPropertyChanged(new PropertyChangedEventArgs("SelectedRecipe"));
            }
        }


        static public RecipeTableVM LoadRecipeCollection(Dictionary<string, Recipe> recipes)
        {
            var table = new RecipeTableVM();

            List<Recipe> sortedList = new List<Recipe>(recipes.Values);
            sortedList.Sort(new RecipeSorter());
            foreach (var recipe in sortedList)
            {
                table.Add(recipe);
            }
            table.SelectedRecipe = table[0];
            return table;
        }
    }
}
