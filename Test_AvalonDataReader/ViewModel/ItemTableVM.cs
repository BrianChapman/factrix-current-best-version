﻿using FactorioDataReader;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_WPFDataReader.ViewModel
{
    public class ItemTableVM : ObservableCollection<Item>
    {

        class ItemSorter : Comparer<Item>
        {
            public override int Compare(Item a, Item b)
            {
                return string.Compare(a.Name, b.Name);
            }
        }


        Item _selectedItem;
        public Item SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                OnPropertyChanged(new PropertyChangedEventArgs("SelectedItem"));
            }
        }


        static public ItemTableVM LoadItemCollection(ItemTable items)
        {
            var table = new ItemTableVM();

            List<Item> sortedList = new List<Item>(items.Values);
            sortedList.Sort(new ItemSorter());
            foreach (var item in sortedList)
            {
                table.Add(item);
            }
            table.SelectedItem = table[0];
            return table;
        }
    }
}
