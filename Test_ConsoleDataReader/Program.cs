﻿using FactorioDataReader;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_ConsoleDataReader
{
    class Program
    {
        static void Main(string[] args)
        {
            var p = new Program();
            p.BuildDataModel();
        }

        Program()
        {
        }

        void BuildDataModel()
        {
            GameData gameData = new GameData();
            gameData.Difficulty = Difficulty.Normal;
            gameData.EnabledMods = null;
            gameData.Locale = "en";

            Dictionary<string, Item> items = gameData.Items;
            Dictionary<string, Recipe> recipes = gameData.Recipes;
            if (gameData.HasErrors)
            {
                foreach(var pair in gameData.FileErrors)
                {
                    Console.WriteLine("ERROR -- {0}: {1}", pair.Key, pair.Value.Message);
                }
            }


            Console.WriteLine("GameData: {0} Items, {1} Recipes", items.Count, recipes.Count);

            foreach(var item in items.Values)
            {
                if (string.IsNullOrWhiteSpace(item.IconPath))
                    Console.WriteLine("{0} missing IconPath", item.Name);
                if (string.IsNullOrWhiteSpace(item.FriendlyName))
                    Console.WriteLine("{0} missing FriendlyName", item.Name);
            }

            foreach (var recipe in recipes.Values)
            {
                if (string.IsNullOrWhiteSpace(recipe.IconPath))
                    Console.WriteLine("{0} missing IconPath", recipe.Name);
                if (string.IsNullOrWhiteSpace(recipe.FriendlyName))
                    Console.WriteLine("{0} missing FriendlyName", recipe.Name);
            }
        }

    }
}
