﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorioDataReader
{
    public class LocaleDictionary : Dictionary<string, Dictionary<string, string>>
    {
        public Dictionary<string, Exception> FailedFiles { get; private set; }
        public bool HasErrors
        {
            get { return FailedFiles.Count > 0; }
        }

        public LocaleDictionary()
        {
            FailedFiles = new Dictionary<string, Exception>();
        }

        public void LoadLocaleFiles(List<Mod> mods, string locale)
        {
            foreach (var mod in mods.Where(m => m.Enabled))
            {
                var localeDir = Path.Combine(mod.dir, "locale", locale);
                if (Directory.Exists(localeDir))
                {
                    foreach (var file in Directory.GetFiles(localeDir, "*.cfg"))
                    {
                        try
                        {
                            using (var fStream = new StreamReader(file))
                            {
                                var currentIniSection = "none";

                                while (!fStream.EndOfStream)
                                {
                                    var line = fStream.ReadLine();
                                    if (line.StartsWith("[") && line.EndsWith("]"))
                                    {
                                        currentIniSection = line.Trim('[', ']');
                                    }
                                    else
                                    {
                                        if (!this.ContainsKey(currentIniSection))
                                            this.Add(currentIniSection, new Dictionary<string, string>());
                                        var split = line.Split('=');
                                        if (split.Count() == 2)
                                            this[currentIniSection][split[0].Trim()] = split[1].Trim();
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            FailedFiles[file] = e;
                        }
                    }
                }
            }
        }
    }
}
