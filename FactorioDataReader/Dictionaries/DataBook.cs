﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorioDataReader
{
    public class DataBook : Dictionary<object, object>
    {
        public T TryGet<T>(object key)
        {
            object thing;
            this.TryGetValue(key, out thing);
            if (thing is T)
                return (T)thing;
            return default(T);
        }

        public int TryGetAmount(object key)
        {
            object thing;
            this.TryGetValue(key, out thing);
            if (thing is double)
            {
                double value = (double)thing;
                int integer = (int)value;
                if (integer != value)
                    throw new Exception("reading non-integer value");

                return integer;
            }
            return 0;
        }

        public static bool KeyValuePairAs<K, V>(KeyValuePair<object, object> kvp,
                                                out K key, out V val)
                                                where K : class
                                                where V : class
        {
            key = kvp.Key as K;
            val = kvp.Value as V;
            if (key == null || val == null)
                return false;
            return true;
        }
    }
}
