﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorioDataReader
{
    public class DataBookKeyComparer : IComparer<object>
    {
        // Key can be either a string or a double.
        // doubles sort before string.
        public int Compare(object a, object b)
        {
            string aString = a as String;
            string bString = b as String;
            double aNum = 0;
            double bNum = 0;
            if (aString == null)
                aNum = (double)a;
            if (bString == null)
                bNum = (double)b;

            if (aString == null && bString == null)
                return aNum.CompareTo(bNum);
            if (aString == null)
                return -1;
            if (bString == null)
                return 1;
            return aString.CompareTo(bString);
        }
    }


}
