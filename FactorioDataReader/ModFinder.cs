﻿using FactorioDataReader.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.IO.Compression;

namespace FactorioDataReader
{
    class ModFinder
    {
        string _dataPath;
        List<Mod> _mods;
        public Dictionary<string, byte[]> _zipHashes;


        public ModFinder()
        {
            _dataPath = Path.Combine(Settings.Default.FactorioPath, "data");
            _mods = new List<Mod>();
            _zipHashes = new Dictionary<string, byte[]>();
        }

    public List<Mod> FindAllMods(List<string> enabledMods) //Vanilla game counts as a mod too.
        {
            if (Directory.Exists(_dataPath))
                foreach (var dir in Directory.EnumerateDirectories(_dataPath))
                    ReadModInfoFile(dir);
            if (Directory.Exists(Settings.Default.FactorioModPath))
            {
                foreach (var dir in Directory.EnumerateDirectories(Settings.Default.FactorioModPath))
                    ReadModInfoFile(dir);
                foreach (var zipFile in Directory.EnumerateFiles(Settings.Default.FactorioModPath, "*.zip"))
                    ReadModInfoZip(zipFile);
            }

            var enabledModsFromFile = new Dictionary<string, bool>();

            var modListFile = Path.Combine(Settings.Default.FactorioModPath, "mod-list.json");
            if (File.Exists(modListFile))
            {
                var json = File.ReadAllText(modListFile);
                dynamic parsedJson = JsonConvert.DeserializeObject(json);
                foreach (var mod in parsedJson.mods)
                {
                    string name = mod.name;
                    var enabled = (bool)mod.enabled;
                    enabledModsFromFile.Add(name, enabled);
                }
            }

            if (enabledMods != null)
            {
                foreach (var mod in _mods)
                    mod.Enabled = enabledMods.Contains(mod.Name);
            }
            else
            {
                var splitModStrings = new Dictionary<string, string>();
                foreach (var s in Settings.Default.EnabledMods)
                {
                    var split = s.Split('|');
                    splitModStrings.Add(split[0], split[1]);
                }
                foreach (var mod in _mods)
                    if (splitModStrings.ContainsKey(mod.Name))
                        mod.Enabled = splitModStrings[mod.Name] == "True";
                    else if (enabledModsFromFile.ContainsKey(mod.Name))
                        mod.Enabled = enabledModsFromFile[mod.Name];
                    else
                        mod.Enabled = true;
            }

            var modGraph = new DependencyGraph(_mods);
            modGraph.DisableUnsatisfiedMods();

            List<Mod> mods = modGraph.SortMods();
            return mods;
        }


        private void ReadModInfoFile(string dir)
        {
            try
            {
                if (!File.Exists(Path.Combine(dir, "info.json")))
                    return;
                var json = File.ReadAllText(Path.Combine(dir, "info.json"));
                ReadModInfo(json, dir);
            }
            catch (Exception)
            {
                ErrorLogging.LogLine($"The mod at '{dir}' has an invalid info.json file");
            }
        }

        private void ReadModInfoZip(string zipFile)
        {
            UnzipMod(zipFile);

            var file =
                Directory.EnumerateFiles(Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(zipFile)),
                    "info.json", SearchOption.AllDirectories).FirstOrDefault();
            if (string.IsNullOrWhiteSpace(file))
                return;
            ReadModInfo(File.ReadAllText(file), Path.GetDirectoryName(file));
        }

        private void ReadModInfo(string json, string dir)
        {
            var newMod = JsonConvert.DeserializeObject<Mod>(json);
            newMod.dir = dir;

            if (!Version.TryParse(newMod.version, out newMod.parsedVersion))
                newMod.parsedVersion = new Version(0, 0, 0, 0);
            ParseModDependencies(newMod);

            _mods.Add(newMod);
        }

        private void UnzipMod(string modZipFile)
        {
            var fullPath = Path.GetFullPath(modZipFile);
            byte[] hash;
            var needsExtraction = false;

            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(fullPath))
                {
                    hash = md5.ComputeHash(stream);
                }
            }

            if (_zipHashes.ContainsKey(fullPath))
            {
                if (!_zipHashes[fullPath].SequenceEqual(hash))
                {
                    needsExtraction = true;
                    _zipHashes[fullPath] = hash;
                }
            }
            else
            {
                needsExtraction = true;
                _zipHashes.Add(fullPath, hash);
            }

            var outputDir = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(modZipFile));

            if (needsExtraction)
            {
                using (var zip = ZipStorer.Open(modZipFile, FileAccess.Read))
                {
                    foreach (var fileEntry in zip.ReadCentralDir())
                        zip.ExtractFile(fileEntry, Path.Combine(outputDir, fileEntry.FilenameInZip));
                }
            }
        }

        private void ParseModDependencies(Mod mod)
        {
            if (mod.Name == "base")
                mod.dependencies.Add("core");

            foreach (var depString in mod.dependencies)
            {
                var token = 0;

                var newDependency = new ModDependency();

                var split = depString.Split(' ');

                if (split[token] == "?")
                {
                    newDependency.Optional = true;
                    token++;
                }

                newDependency.ModName = split[token];
                token++;

                if (split.Count() == token + 2)
                {
                    switch (split[token])
                    {
                    case "=":
                        newDependency.VersionType = DependencyType.EqualTo;
                        break;
                    case ">":
                        newDependency.VersionType = DependencyType.GreaterThan;
                        break;
                    case ">=":
                        newDependency.VersionType = DependencyType.GreaterThanOrEqual;
                        break;
                    }
                    token++;

                    if (!Version.TryParse(split[token], out newDependency.Version))
                        newDependency.Version = new Version(0, 0, 0, 0);
                    token++;
                }

                mod.parsedDependencies.Add(newDependency);
            }
        }



    }
}
