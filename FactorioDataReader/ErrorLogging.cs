﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorioDataReader
{
    public static class ErrorLogging
    {
        public static void LogLine(String message)
        {
            try
            {
                File.AppendAllText(Path.Combine(FilePaths.StartupPath, "errorlog.txt"), message + "\n");
            }
            catch { } //Not good.
        }
    }
}
