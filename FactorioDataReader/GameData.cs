﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorioDataReader
{
    public class GameData
    {
        public List<string> EnabledMods { get; set; }
        public Difficulty Difficulty { get; set; }
        public string Locale {get; set; }

        private void ConfirmDataLoaded()
        {
            if (!_dataLoaded)
            {
                _dataLoaded = true;
                LoadGameData();
            }
        }

        RecipeTable _recipes;
        public RecipeTable Recipes
        {
            get
            {
                ConfirmDataLoaded();
                return _recipes;
            }
            private set { _recipes = value; }
        }

        ItemGroupTable _itemGroups;
        public ItemGroupTable ItemGroups
        {
            get
            {
                ConfirmDataLoaded();
                return _itemGroups;
            }
            private set { _itemGroups = value; }
        }

        ItemSubgroupTable _itemSubgroups;
        public ItemSubgroupTable ItemSubgroups
        {
            get
            {
                ConfirmDataLoaded();
                return _itemSubgroups;
            }
            private set { _itemSubgroups = value; }
        }

        ItemTable _items;
        public ItemTable Items
        {
            get
            {
                ConfirmDataLoaded();
                return _items;
            }
            private set { _items = value; }
        }


        LocaleDictionary _localeDictionary;
        public LocaleDictionary LocaleData
        {
            get
            {
                ConfirmDataLoaded();
                return _localeDictionary;
            }
            private set { _localeDictionary = value; }
        }


        public Dictionary<string, Exception> FileErrors { get; private set; }
        public bool HasErrors
        {
            get { return FileErrors.Count > 0; }
        }

        bool _dataLoaded = false;

        public GameData()
        {
            FileErrors = new Dictionary<string, Exception>();
        }

        private void LoadGameData()
        {
            LuaInstance luaInstance = new LuaInstance(EnabledMods, Locale);

            // Load all the data from Lua
            DataBook gameBook = luaInstance.LoadLuaData();
            if (luaInstance.HasErrors)
                PullUpErrors(luaInstance);

            //string pxml = PseudoXmlFormatter.Format(gameBook);

            // Load the Localiziation information.
            LocaleData = new LocaleDictionary();
            LocaleData.LoadLocaleFiles(luaInstance.Mods, Locale);
            if (LocaleData.HasErrors)
                PullUpErrors(LocaleData);

            // Load the Item Groups and SubGroups
            ItemGroups = new ItemGroupTable();
            ItemGroups.LoadItemGroupTable(gameBook);

            ItemSubgroups = new ItemSubgroupTable();
            ItemSubgroups.LoadItemSubgroupTable(gameBook, ItemGroups);

            // Load the recipes
            Recipes = new RecipeTable(Difficulty);
            Recipes.LoadRecipeData(gameBook);

            // Collect the items in/out of all the recipes
            List<string> allItemNames = CollectAllItemNamesInRecipes();

            // Collect all the Items on the list.
            // Ans populate the item subgroups
            Items = new ItemTable();
            Items.LoadItemData(gameBook, allItemNames, ItemSubgroups);

            // Load the icons.
            // This is a seperate pass because the recipes often use the Item icons
            SetIcons(luaInstance.Mods);

            // Add friendlynames uses the locale information.
            SetFriendlyNames();

            SetRecipeCategories();

            // Write the creating recipes into each Item.
            SetItemRecipes();

            // Add the Item object pointers into each Recipe
            SetItemObjectsOnRecipes();
        }

        List<string> CollectAllItemNamesInRecipes()
        {
            HashSet<string> allItems = new HashSet<string>();
            foreach (Recipe recipe in Recipes.Values)
            {
                foreach (ItemResource resource in recipe.Ingredients)
                    allItems.Add(resource.ItemName);
                foreach (ItemResource resource in recipe.Results)
                    allItems.Add(resource.ItemName);
            }

            List<string> itemNamesSorted = new List<string>(allItems);
            itemNamesSorted.Sort();
            return itemNamesSorted;
        }

        void SetIcons(List<Mod> mods)
        {
            foreach(ItemGroup group in ItemGroups.Values)
            {
                group.IconPath = FilePaths.NormalizeModPath(group.IconPath, mods);
            }

            foreach (Item item in Items.Values)
            {
                if (string.IsNullOrWhiteSpace(item.IconPath))
                    throw new Exception("Missing Item Icon");
                item.IconPath = FilePaths.NormalizeModPath(item.IconPath, mods);
            }

            foreach(Recipe recipe in Recipes.Values)
            {
                if(!string.IsNullOrWhiteSpace(recipe.IconPath))
                {
                    recipe.IconPath = FilePaths.NormalizeModPath(recipe.IconPath, mods);
                }
                else
                {
                    // If there is no Icon, use the Icon of the Result Item.
                    if (recipe.Results.Count == 1)
                    {
                        string name = recipe.Results[0].ItemName;
                        Item item = Items[name];
                        recipe.IconPath = item.IconPath;  // already normalized
                    }
                    else
                    {
                        // If there is more than one Result
                        // Then the Recipe should have come with an Icon.
                        throw new Exception("Missing Recipe Icon");
                    }
                }
            }
        }

        string LookupLocalizedName(string systemName, string sectionName)
        {
            if (LocaleData.ContainsKey(sectionName) && LocaleData[sectionName].ContainsKey(systemName))
                return LocaleData[sectionName][systemName];
            return null;
        }

        void SetFriendlyNames()
        {
            List<String> itemLocaleCategories = new List<String> { "item-name", "fluid-name", "entity-name", "equipment-name" };


            foreach (ItemGroup group in ItemGroups.Values)
            {
                group.FriendlyName = group.Name;
                string locName = LookupLocalizedName(group.Name, "item-group-name");
                if (locName != null)
                    group.FriendlyName = locName;
            }

            foreach (Item item in Items.Values)
            {
                string friendlyName = item.Name;
                foreach (String category in itemLocaleCategories)
                {
                    string locName = LookupLocalizedName(item.Name, category);
                    if (locName != null)
                    {
                        friendlyName = locName;
                        break;
                    }
                }
                item.FriendlyName = friendlyName;
            }

            foreach(Recipe recipe in Recipes.Values)
            {
                string friendlyName = recipe.Name;

                // First, if it has a special Friendly name use that.
                string locName = LookupLocalizedName(recipe.Name, "recipe-name");
                if (locName != null)
                    recipe.FriendlyName = locName;

                // otherwise try the FriendlyName of the Result
                else if (recipe.Results.Count == 1)
                {
                    string firstResultName = recipe.Results[0].ItemName;
                    Item item = Items[firstResultName];
                    friendlyName = item.FriendlyName;
                }
                recipe.FriendlyName = friendlyName;
            }
        }

        void SetRecipeCategories()
        {
            foreach(Recipe recipe in Recipes.Values)
            {
                if(string.IsNullOrEmpty(recipe.Category))
                {
                    // If there is no Category, try using the Item type of the Result Item.
                    if (recipe.Results.Count == 1)
                    {
                        string name = recipe.Results[0].ItemName;
                        Item item = Items[name];
                        recipe.Category = item.SubgroupName;
                    }
                    else
                    {
                        throw new Exception("Missing Recipe Category");
                    }
                }
            }
        }

        void SetItemRecipes()
        {
            foreach(Recipe recipe in Recipes.Values)
            {
                foreach(ItemResource resource in recipe.Results)
                {
                    Item item = Items[resource.ItemName];
                    item.CreatedByRecipes.Add(recipe);
                }
                foreach (ItemResource resource in recipe.Ingredients)
                {
                    Item item = Items[resource.ItemName];
                    item.UsedByRecipes.Add(recipe);
                }
            }
        }

        void SetItemObjectsOnRecipes()
        {
            foreach (Recipe recipe in Recipes.Values)
            {
                foreach (ItemResource resource in recipe.Ingredients)
                    resource.Item = Items[resource.ItemName];
                foreach (ItemResource resource in recipe.Results)
                    resource.Item = Items[resource.ItemName];
            }
        }

        void PullUpErrors(LocaleDictionary localeData)
        {
            foreach(var pair in localeData.FailedFiles)
            {
                FileErrors.Add(pair.Key, pair.Value);
            }
        }

        void PullUpErrors(LuaInstance lua)
        {
            foreach (var pair in lua.FailedPathDirectories)
            {
                FileErrors.Add(pair.Key, pair.Value);
            }
            foreach (var pair in lua.FailedFiles)
            {
                FileErrors.Add(pair.Key, pair.Value);
            }
        }

    }
}
