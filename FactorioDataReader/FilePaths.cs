﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorioDataReader
{
    class FilePaths
    {
        static string _startupPath = null;
        public static string StartupPath
        {
            get
            {
                if (_startupPath == null)
                {
                    string basePath = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
                    if (basePath.StartsWith("file:///"))
                    {
                        basePath = basePath.Substring(8);
                    }
                    _startupPath = basePath;
                }
                return _startupPath;
            }
        }

        static public string NormalizeModPath(string path, List<Mod> mods)
        {
            if (string.IsNullOrEmpty(path))
                return null;

            var fullPath = "";
            if (File.Exists(path))
            {
                fullPath = path;
            }
            else if (File.Exists(Path.Combine(FilePaths.StartupPath, path)))
            {
                fullPath = Path.Combine(FilePaths.StartupPath, path);
            }
            else
            {
                var splitPath = path.Split('/');
                splitPath[0] = splitPath[0].Trim('_');

                if (mods.Any(m => m.Name == splitPath[0]))
                    fullPath = mods.First(m => m.Name == splitPath[0]).dir;

                if (!string.IsNullOrEmpty(fullPath))
                {
                    // The first part of the split == the last part of the Mod dir
                    // so skip the first part of the split.
                    for (var i = 1; i < splitPath.Count(); i++)
                        fullPath = Path.Combine(fullPath, splitPath[i]);
                }
            }
            return fullPath;
        }
    }
}
