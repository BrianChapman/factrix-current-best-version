﻿using FactorioDataReader.Properties;
using Newtonsoft.Json;
using NLua;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorioDataReader
{
    class LuaInstance
    {
        NLua.Lua _luaInstance;

        List<string> _enabledMods;
        string _locale;
        string _dataPath;

        public List<Mod> Mods { get; private set; }

        public Dictionary<string, Exception> FailedFiles { get; private set; }
        public Dictionary<string, Exception> FailedPathDirectories { get; private set; }

        public bool HasErrors
        {
            get { return FailedFiles.Count > 0 || FailedPathDirectories.Count > 0; }
        }


        public LuaInstance(List<string> enabledMods, string locale)
        {
            _enabledMods = enabledMods;
            _locale = locale;
            _dataPath = Path.Combine(Settings.Default.FactorioPath, "data");
            FailedFiles = new Dictionary<string, Exception>();
            FailedPathDirectories = new Dictionary<string, Exception>();
        }

        public DataBook LoadLuaData()
        {
            SetupConfiguration();
            LoadAndExecuteGameLua();

            LuaTable luaTable = _luaInstance.GetTable("data.raw") as NLua.LuaTable;
            DataBook dataTable = DeepCopyTable(luaTable);

            return dataTable;
        }

        private DataBook DeepCopyTable(NLua.LuaTable root)
        {
            DataBook book = new DataBook();

            var tableEnumerator = root.GetEnumerator();
            while (tableEnumerator.MoveNext())
            {
                object key = tableEnumerator.Key;
                object value = tableEnumerator.Value;
                if (value is NLua.LuaTable)
                {
                    var subTable = DeepCopyTable(value as NLua.LuaTable);
                    book.Add(key, subTable);
                }
                else
                {
                    book.Add(key, value);
                }
            }
            return book;
        }


        private void SetupConfiguration()
        {
            //I changed the name of the variable, so this copies the value over for people who are upgrading their Foreman version
            if (Properties.Settings.Default.FactorioPath == "" && Properties.Settings.Default.FactorioDataPath != "")
            {
                Properties.Settings.Default["FactorioPath"] = Path.GetDirectoryName(Properties.Settings.Default.FactorioDataPath);
                Properties.Settings.Default["FactorioDataPath"] = "";
            }

            if (!Directory.Exists(Properties.Settings.Default.FactorioPath))
            {
                foreach (String defaultPath in new String[]{
                                                          Path.Combine(Environment.GetEnvironmentVariable("PROGRAMFILES(X86)"), "Factorio"),
                                                          Path.Combine(Environment.GetEnvironmentVariable("ProgramW6432"), "Factorio"),
                                                          Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Applications", "factorio.app", "Contents")}) //Not actually tested on a Mac
                {
                    if (Directory.Exists(defaultPath))
                    {
                        Properties.Settings.Default["FactorioPath"] = defaultPath;
                        Properties.Settings.Default.Save();
                        break;
                    }
                }
            }

            if (!Directory.Exists(Properties.Settings.Default.FactorioPath))
            {
                throw new Exception("FactorioPath not correct.");
                //using (DirectoryChooserForm form = new DirectoryChooserForm(""))
                //{
                //    if (form.ShowDialog() == DialogResult.OK)
                //    {
                //        Properties.Settings.Default["FactorioPath"] = form.SelectedPath; ;
                //        Properties.Settings.Default.Save();
                //    }
                //    else
                //    {
                //        Close();
                //        Dispose();
                //        return;
                //    }
                //}
            }

            if (!Directory.Exists(Properties.Settings.Default.FactorioModPath))
            {
                if (Directory.Exists(Path.Combine(Properties.Settings.Default.FactorioPath, "mods")))
                {
                    Properties.Settings.Default["FactorioModPath"] = Path.Combine(Properties.Settings.Default.FactorioPath, "mods");
                }
                else if (Directory.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "factorio", "mods")))
                {
                    Properties.Settings.Default["FactorioModPath"] = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "factorio", "mods");
                }
            }

            if (Properties.Settings.Default.EnabledMods == null)
                Properties.Settings.Default.EnabledMods = new StringCollection();

            //LanguageDropDown.Items.AddRange(DataCache.Languages.ToArray());
            //LanguageDropDown.SelectedItem = DataCache.Languages.FirstOrDefault(l => l.Name == Properties.Settings.Default.Language);

        }


        private void LoadAndExecuteGameLua()
        {
            _luaInstance = new NLua.Lua();

            var modFinder = new ModFinder();
            Mods = modFinder.FindAllMods(_enabledMods);

            AddLuaPackagePath(_luaInstance, Path.Combine(_dataPath, "core", "lualib")); // Core lua functions
            var basePackagePath = _luaInstance["package.path"] as string;

            var dataloaderFile = Path.Combine(_dataPath, "core", "lualib", "dataloader.lua");
            try
            {
                _luaInstance.DoFile(dataloaderFile);
            }
            catch (Exception e)
            {
                FailedFiles[dataloaderFile] = e;
                ErrorLogging.LogLine(
                    $"Error loading dataloader.lua. This file is required to load any values from the prototypes. Message: '{e.Message}'");
                return;
            }

            _luaInstance.DoString(@"
                    function module(modname,...)
                    end
    
                    require ""util""
                    util = {}
                    util.table = {}
                    util.table.deepcopy = table.deepcopy
                    util.multiplystripes = multiplystripes
                    util.by_pixel = by_pixel
                    util.format_number = format_number
                    util.increment = increment

                    function log(...)
                    end

                    defines = {}
                    defines.difficulty_settings = {}
                    defines.difficulty_settings.recipe_difficulty = {}
                    defines.difficulty_settings.technology_difficulty = {}
                    defines.difficulty_settings.recipe_difficulty.normal = 1
                    defines.difficulty_settings.technology_difficulty.normal = 1
                    defines.direction = {}
                    defines.direction.north = 1
                    defines.direction.east = 2
                    defines.direction.south = 3
                    defines.direction.west = 4
");

            foreach (var filename in new[] { "data.lua", "data-updates.lua", "data-final-fixes.lua" })
            {
                foreach (var mod in Mods.Where(m => m.Enabled))
                {
                    // Mods use relative paths, but if more than one mod is in package.path
                    // at once this can be ambiguous
                    _luaInstance["package.path"] = basePackagePath;
                    AddLuaPackagePath(_luaInstance, mod.dir);

                    // Because many mods use the same path to refer to different files,
                    // we need to clear the 'loaded' table so Lua doesn't think they're already loaded
                    _luaInstance.DoString(@"
                            for k, v in pairs(package.loaded) do
                                package.loaded[k] = false
                            end");

                    var dataFile = Path.Combine(mod.dir, filename);
                    if (File.Exists(dataFile))
                    {
                        try
                        {
                            _luaInstance.DoFile(dataFile);
                        }
                        catch (Exception e)
                        {
                            FailedFiles[dataFile] = e;
                        }
                    }
                }
            }
        }

        private void AddLuaPackagePath(NLua.Lua lua, string dir)
        {
            try
            {
                var luaCommand = $"package.path = package.path .. ';{dir}{Path.DirectorySeparatorChar}?.lua'";
                luaCommand = luaCommand.Replace("\\", "\\\\");
                lua.DoString(luaCommand);
            }
            catch (Exception e)
            {
                FailedPathDirectories[dir] = e;
            }
        }


    }
}
