﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorioDataReader
{
    [DebuggerDisplay("{Name}")]
    public class Item
    {
        public string Name { get; private set; }
        public string SubgroupName { get; set; }
        public ItemSubgroup Subgroup { get; set; }
        public string IconPath { get; set; }

        public HashSet<Recipe> CreatedByRecipes { get; private set; }
        public HashSet<Recipe> UsedByRecipes { get; private set; }
        public String FriendlyName { get; set; }

        internal Item(string name, string subgroup, string iconPath)
        {
            Name = name;
            SubgroupName = subgroup;
            IconPath = iconPath;
            CreatedByRecipes = new HashSet<Recipe>();
            UsedByRecipes = new HashSet<Recipe>();
        }
    }
}
