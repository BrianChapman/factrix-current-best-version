﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorioDataReader
{
    [DebuggerDisplay("{Name}")]
    public class Recipe: INotifyPropertyChanged
    {
        public String Name { get; private set; }
        public List<ItemResource> Results { get; private set; }
        public List<ItemResource> Ingredients { get; private set; }
        public float EnergyRequired { get; set; }
        public Boolean Enabled { get; set; }

        public string IconPath { get; set; }
        public String Category { get; set; }
        //public float BaseRate { get { return 1.0f / BaseTime; } }

        internal Recipe(string name, List<ItemResource> ingredients, List<ItemResource> results)
        {
            Name = name;
            Ingredients = ingredients;
            Results = results;
        }

        private String _friendlyName;
        public String FriendlyName
        {
            get { return _friendlyName; }
            set
            {
                _friendlyName = value;
                Notify("FriendlyName");
                Notify("Description");
            }
        }

        public static String TimeText(float time)
        {
            String rate = (1.0f / time).ToString();
            if (rate.Length > 5)
                rate = rate.Substring(0, 5);
            String timeText = "Base Time: " + rate + "/sec";
            if(time > 1.0)
                timeText += " (" + time + " secs)";
            return timeText;
        }

        private float _baseTime;
        public float BaseTime
        {
            get { return _baseTime; }
            set
            {
                _baseTime = value;
                Notify("BaseTime");
                //Notify("BaseRate");
                Notify("Description");
            }
        }


        public String Description
        {
            get
            {
                if (BaseTime == 0)
                    return FriendlyName;
                return FriendlyName + ": " + TimeText(BaseTime);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void Notify(string propertyName)
        {
            var eventHandler = PropertyChanged;
            if(eventHandler != null)
            {
                eventHandler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
