﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorioDataReader
{
    [DebuggerDisplay("{Amount} - {ItemName}")]
    public class ItemResource
    {
        public string ItemName { get; private set; }
        public int Amount { get; private set; }

        public Item Item { get; set; }

        public ItemResource(string itemName, int amount)
        {
            ItemName = itemName;
            Amount = amount;
        }
    }

    public class ItemResourceTable : Dictionary<string, ItemResource>
    {

    }
}
