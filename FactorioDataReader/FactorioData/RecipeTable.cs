﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorioDataReader
{
    public enum Difficulty { Normal, Expensive };

    public class RecipeTable: Dictionary<string, Recipe>
    {
        public Difficulty Difficulty { get; private set; }

        public RecipeTable(Difficulty difficulty = Difficulty.Normal)
        {
            Difficulty = difficulty;
        }

        public void LoadRecipeData(DataBook bigBook)
        {
            List<Recipe> sortedRecipeList = new List<Recipe>();
            DataBook recipes = bigBook["recipe"] as DataBook;

            foreach (var recipePair in recipes)
            {
                string name = recipePair.Key as string;
                DataBook recipeSection = recipePair.Value as DataBook;

                DataBook difficultySection = GetDifficultySection(recipeSection);

                List<ItemResource> ingredients = GetIngredients(difficultySection);
                List<ItemResource> results = GetResults(difficultySection);

                var recipe = new Recipe(name, ingredients, results);

                float energy = GetEnergyRequired(difficultySection);
                if (energy == 0)
                    energy = 0.5f;
                recipe.EnergyRequired = energy;
                recipe.BaseTime = energy;

                // The Icon is often missing and we just  use the
                // the icon for the result item. (filled in later)
                // But sometimes things are complicated (multiple results)
                // so the recipe has an Icon.   (someitimes multiple icons);
                string iconPath = GetIconPath(difficultySection);
                recipe.IconPath = iconPath;

                recipe.Category = difficultySection.TryGet<string>("category");

                sortedRecipeList.Add(recipe);
            }

            sortedRecipeList.Sort(new RecipeSorter());

            this.Clear();
            foreach (var recipe in sortedRecipeList)
                this.Add(recipe.Name, recipe);
        }


        private float GetEnergyRequired(DataBook recipe)
        {
            double energyRequired = recipe.TryGet<double>("energy_required");
            return (float)energyRequired;
        }

        private string GetIconPath(DataBook recipe)
        {
            string iconPath = recipe.TryGet<string>("icon");
            if(iconPath == null)
            {
                DataBook icons = recipe.TryGet<DataBook>("icons");
                if (icons != null)
                {
                    DataBook icons1 = icons[1.0] as DataBook;
                    iconPath = icons1["icon"] as string;
                }
            }
            return iconPath;  // often null;
        }

        private DataBook GetDifficultySection(DataBook recipe)
        {
            string diffName = (Difficulty == Difficulty.Normal) ? "normal" : "expensive";
            DataBook diffSection = recipe.TryGet<DataBook>(diffName);
            if (diffSection != null)
                return diffSection as DataBook;
            else
                return recipe;
        }


        private List<ItemResource> GetIngredients(DataBook diffSection)
        {
            DataBook ingredientList = diffSection.TryGet<DataBook>("ingredients");
            if (ingredientList == null)
                throw new Exception("missing ingredients");

            var result = new List<ItemResource>();

            foreach (var i in ingredientList.Values)
            {
                DataBook ingred = i as DataBook;
                string ingredientName = ingred.TryGet<string>(1.0);
                int ingredientAmount;
                ItemResource resource = null;
                if (ingredientName != null)
                {
                    ingredientAmount = ingred.TryGetAmount(2.0);
                }
                else
                {
                    ingredientName = ingred.TryGet<String>("name");
                    if (ingredientName != null)
                        ingredientAmount = ingred.TryGetAmount("amount");
                    else
                        throw new Exception("missing recipe ingredient spec");
                }
                resource = new ItemResource(ingredientName, ingredientAmount);
                result.Add(resource);
            }
            return result;
        }


        // usually one thing but sometimes multiple
        // see:  <recipe name='advanced-oil-processing'
        private List<ItemResource> GetResults(DataBook recipe)
        {
            List<ItemResource> result = new List<ItemResource>();
            string resultName = recipe.TryGet<String>("result");
            if (resultName != null)
            {
                int resultAmount = recipe.TryGetAmount("result_count");
                if (resultAmount == 0)
                    resultAmount = 1;
                var resource = new ItemResource(resultName, resultAmount);
                result.Add(resource);
                return result;
            }

            DataBook resultList = recipe.TryGet<DataBook>("results");
            if (resultList == null)
                throw new Exception("missing recipe result(s)");

            foreach (var i in resultList.Values)
            {
                DataBook ingred = i as DataBook;
                resultName = ingred.TryGet<string>(1.0);
                int resultAmount;
                if (resultName != null)
                {
                    resultAmount = ingred.TryGetAmount(2.0);
                }
                else
                {
                    resultName = ingred.TryGet<String>("name");
                    if (resultName != null)
                    {
                        resultAmount = ingred.TryGetAmount("amount");
                    }
                    else
                        throw new Exception("missing recipe result spec");
                }
                var resource = new ItemResource(resultName, resultAmount);
                result.Add(resource);
            }

            return result;
        }

        class RecipeSorter : Comparer<Recipe>
        {
            public override int Compare(Recipe a, Recipe b)
            {
                return string.Compare(a.Name, b.Name);
            }
        }

    }
}
