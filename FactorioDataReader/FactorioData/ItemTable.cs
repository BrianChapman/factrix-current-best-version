﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorioDataReader
{
    public class ItemTable : Dictionary<string, Item> 
    {
        private static List<string> _itemSectionNames = new List<string> {
            "item",
            "ammo",
            "fluid",
            "gun",
            "armor",
            "capsule",
            "module",
            "tool",
            "item-with-entity-data",
            "mining-tool",
            "repair-tool",
            "rail-planner"
        };

        public void LoadItemData(DataBook bigBook, List<string> itemNames, ItemSubgroupTable subgroups )
        {
            this.Clear();
            foreach (string itemName in itemNames)
            {
                Item item = FindAndCreateItem(itemName, bigBook);
                this.Add(item.Name, item);
                ItemSubgroup subgroup = subgroups[item.SubgroupName];
                item.Subgroup = subgroup;
                subgroup.Items.Add(item);
            }
        }


        Item FindAndCreateItem(string itemName, DataBook bigBook)
        {
            DataBook itemSection = null;
            Item item = null;
            foreach (string sectionName in _itemSectionNames)
            {
                DataBook section = bigBook.TryGet<DataBook>(sectionName);
                itemSection = section.TryGet<DataBook>(itemName);
                if (itemSection != null)
                {
                    item = CreateItem(itemSection);
                    return item;
                }
            }

            // If the item wasn't where we normaly look.
            // Search the entire book (top level) and report what you find.
            string name = SearchAllSectionsForItem(itemName, bigBook, out item);
            return item;
        }


        string SearchAllSectionsForItem(string itemName, DataBook bigBook, out Item item)
        {
            string sectionKey = null;
            DataBook section;
            List<String> foundSectionKeys = new List<string>();
            List<Item> foundItems = new List<Item>();
            foreach (var bookPair in bigBook)
            {
                if (!DataBook.KeyValuePairAs<string, DataBook>(bookPair, out sectionKey, out section))
                    continue;

                // skip the sections we know hold items
                if (_itemSectionNames.Contains(sectionKey))
                    continue;

                // skip the recipe section, because the name might appear there.
                if (0 == sectionKey.CompareTo("recipe"))
                    continue;

                // Look for the item in each top level section.
                var itemSection = section.TryGet<DataBook>(itemName);
                if (itemSection == null)
                    continue;

                Item newItem = CreateItem(itemSection);
                if (newItem != null)
                {
                    foundSectionKeys.Add(sectionKey);
                    foundItems.Add(newItem);
                }
            }

            if (foundSectionKeys.Count == 0)
                throw new Exception(string.Format("Missing Item '{0}' was not found", itemName));
            else if (foundSectionKeys.Count == 1)
            {
                Debug.WriteLine(string.Format("Missing Item '{0}' was found in section of {1}.",
                    foundItems[0].Name, foundSectionKeys[0]));
            }
            else if (foundSectionKeys.Count > 1)
            {
                Debug.Write(string.Format("Missing Item '{0}' was found mulitple times: ", foundItems[0].Name));
                foreach (string key in foundSectionKeys)
                    Debug.Write(string.Format(" / {0}", key));
                Debug.WriteLine(string.Empty);
            }
            item = foundItems[0];
            return foundSectionKeys[0];
        }

        Item CreateItem(DataBook itemBook)
        {
            string name = itemBook.TryGet<string>("name");
            string type = itemBook.TryGet<string>("type");
            string subgroup = itemBook.TryGet<string>("subgroup");
            if (subgroup == null)
            {
                //Debug.WriteLine(string.Format("Item '{0}' is type {1}, subgroup '{2}", name, type, subgroup));
                subgroup = type;    // the fluids do this.
            }

            string iconPath = itemBook.TryGet<string>("icon");
            if(iconPath == null)
            {
                // if there are multiple icons, not just one, pick the first one.
                DataBook iconBook = itemBook.TryGet<DataBook>("icons");
                if(iconBook != null)
                    iconPath = iconBook.TryGet<DataBook>(1.0).TryGet<string>("icon");
            }

            if (subgroup == null)
                throw new Exception(string.Format("Item subgroup is missing on {0}", name));

            if (iconPath == null)
                throw new Exception(string.Format("Item IconPath is missing on {0}", name));

            var item = new Item(name, subgroup, iconPath);
            return item;
        }
    }
}
