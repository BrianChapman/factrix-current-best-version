﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorioDataReader
{
    public class ItemGroup
    {
        public String Name { get; }
        public String Order { get; }
        public String IconPath { get; set; }
        public String FriendlyName { get; set; }
        public List<ItemSubgroup> Subgroups { get; }

        public ItemGroup(string name, string icon, string order)
        {
            Name = name;
            IconPath = icon;
            Order = order;
            Subgroups = new List<ItemSubgroup>();
        }
    }

    public class ItemSubgroup
    {
        public String Name { get; set; }
        public string Order { get; set; }
        public ItemGroup ParentGroup { get; }
        public List<Item> Items { get; }

        public ItemSubgroup(string name, string order, ItemGroup parentGroup)
        {
            Name = name;
            Order = order;
            ParentGroup = parentGroup;
            parentGroup.Subgroups.Add(this);
            Items = new List<Item>();
        }
    }

    public class ItemGroupTable : Dictionary<string, ItemGroup>
    {
        public void LoadItemGroupTable(DataBook bigBook)
        {
            DataBook itemGroupSection = bigBook["item-group"] as DataBook;

            foreach (var kvp in itemGroupSection)
            {
                string key;
                DataBook value;
                if (!DataBook.KeyValuePairAs<string, DataBook>(kvp, out key, out value))
                    continue;

                string name = value.TryGet<string>("name");
                string icon = value.TryGet<string>("icon");
                string order = value.TryGet<string>("order");

                ItemGroup group = new ItemGroup(name, icon, order);
                Debug.Assert(name == key);
                this.Add(name, group);
            }
        }
    }

    public class ItemSubgroupTable : Dictionary<string, ItemSubgroup>
    {
        public void LoadItemSubgroupTable(DataBook bigBook, ItemGroupTable groupTable)
        {
            DataBook itemSubgroupSection = bigBook["item-subgroup"] as DataBook;

            foreach(var kvp in itemSubgroupSection)
            {
                string key;
                DataBook value;
                if (!DataBook.KeyValuePairAs<string, DataBook>(kvp, out key, out value))
                    continue;

                string name = value.TryGet<string>("name");
                string order = value.TryGet<string>("order");
                string groupName = value.TryGet<string>("group");

                ItemGroup parentGroup = groupTable[groupName];
                ItemSubgroup subgroup = new ItemSubgroup(name, order, parentGroup);
                Debug.Assert(name == key);
                this.Add(name, subgroup);
            }
        }
    }
}
