﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorioDataReader
{
    class PseudoXmlFormatter
    {
        class Node
        {
            public string Name;
            public Node Parent;
            public Node(string name, Node parent) { Name = name;  Parent = parent; }
            public string Path
            {
                get { return (Parent == null) ? Name : Parent.Path + "/" + Name; }
            }
            public int Depth
            {
                get { return (Parent == null) ? 0 : Parent.Depth + 1; }
            }
        }

        static public string Format(DataBook book)
        {
            StringBuilder sb = new StringBuilder();
            Node node = new Node(String.Empty, null);
            sb.AppendLine("<Top>\n");
            sb.Append(Format2R(book, node, false));
            sb.AppendLine("</Top>");
            return sb.ToString();
        }

        static string[] exclude_toplevel_nodes = new string[]
        {
            "achievement",
            "ambient-sound",
            "build-entity-achievement",
            "character-corpse",
            "cliff",
            "combat-robot-count",
            "construct-with-robots-achievement",
            "corpse",
            "damage-type",
            "decider-combinator",
            "deconstruct-with-robots-achievement",
            "deconstructible-tile-proxy",
            "deconstruction-item",
            "decorative",
            "deliver-by-robots-achievement",
            "dont-build-entity-achievement",
            "dont-craft-manually-achievement",
            "dont-use-entity-in-energy-production-achievement",
            "electric-energy-interface",
            "electric-pole",
            "entity-ghost",
            "explosion",
            "finish-the-game-achievement",
            "fire",
            "fish",
            "flame-thrower-explosion",
            "flying-text",
            "font",
            "god-controller",
            "group-attack-achievement",
            "gui-style",
            "kill-achievement",
            "leaf-particle",
            "map-gen-presets",
            "map-settings",
            "market",
            "noise-expression",
            "noise-layer",
            "offshore-pump",
            "optimized-decorative",
            "particle-source",
            "particle",
            "pipe-to-ground",
            "pipe",
            "player",
            "player-damaged-achievement",
            "produce-achievement",
            "produce-per-hour-achievement",
            "rocket-silo-rocket-shadow",
            "programmable-speaker",
            "projectile",
            "pump",
            "radar",
            "rail-chain-signal",
            "rail-planner",
            "rail-remnants",
            "rail-signal",
            "selection-tool",
            "simple-entity-with-force",
            "simple-entity-with-owner",
            "simple-entity",
            "smoke-with-trigger",
            "smoke",
            "sticker",
            "tile-ghost",
            "tile",
            "train-path-achievement",
            "tree",
            "train-stop",
            "transport-belt",
            "trivial-smoke",
            "turret",
            "tutorial",
            "underground-belt",
            "unit-spawner",
            "unit",
            "utility-constants",
            "utility-sounds",
            "utility-sprites",
            "virtual-signal",
            "wall"
        };

        static string Format2R(DataBook book, Node parent, bool skipAttributes)
        {
            StringBuilder sb = new StringBuilder();
            DataBookKeyComparer comparer = new DataBookKeyComparer();

            List<Object> sortedKeys = new List<object>(book.Keys);
            sortedKeys.Sort(comparer);
            string parentPath = parent.Path;
            int depth = parent.Depth;

            foreach (var key in sortedKeys)
            {
                if (depth == 0 && key is string)
                {
                    string keystring = (string)key;
                    bool skip = false;
                    foreach(string notusefull in exclude_toplevel_nodes)
                    {
                        if (notusefull == (string)key)
                        {
                            skip = true;
                            break;
                        }
                    }
                    if (skip)
                        continue;
                }
                object value = book[key];
                if (value is DataBook)
                {
                    if (key is string)
                    {
                        sb.Append(Indent(depth));
                        sb.Append(string.Format("<{0} ", key));
                        Node node = new Node(key.ToString(), parent);
                        sb.Append(GetAttributeChildren((DataBook)value));
                        sb.AppendLine(string.Format(" parent-path=\'{0}\'>", parent.Path == string.Empty ? "/" : parent.Path));

                        sb.Append(Format2R((DataBook)value, node, true));

                        sb.Append(Indent(depth));
                        sb.AppendLine(string.Format("</{0}>", key));
                    }
                    else
                    {
                        sb.Append(Indent(depth));
                        sb.AppendLine(string.Format("<list>"));
                        Node node = new Node(key.ToString(), parent);
                        sb.Append(Format2R((DataBook)value, node, false));

                        sb.Append(Indent(depth));
                        sb.AppendLine(string.Format("</list>"));
                    }
                }
                else
                {
                    if (key is String)
                    {
                        if (!skipAttributes)
                        {
                            sb.Append(Indent(depth));
                            sb.Append(string.Format("{0}{1}=", "{", key));

                            if (value is string)
                                sb.Append(string.Format("\'{0}\'", value));
                            else
                                sb.Append(string.Format("{0}", value));
                            sb.AppendLine("}");
                        }
                    }
                    else
                    {
                        sb.Append(Indent(depth));
                        sb.AppendLine(string.Format("<{0}/>", value));
                    }
                }

            }
            return sb.ToString();
        }

        static string GetAttributeChildren(DataBook book)
        {
            StringBuilder sb = new StringBuilder();
            DataBookKeyComparer comparer = new DataBookKeyComparer();

            List<Object> sortedKeys = new List<object>(book.Keys);
            sortedKeys.Sort(comparer);
            foreach (var key in sortedKeys)
            {
                if (key is string && book[key] is string)
                    sb.Append(string.Format(" {0}=\'{1}\'", key, book[key]));
            }
            return sb.ToString();
        }

        static string Indent(int depth)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < depth; ++i)
                sb.Append("  ");
            return sb.ToString();
        }

    }
}
