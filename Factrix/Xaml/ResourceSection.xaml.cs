﻿using Factrix.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Factrix.Xaml
{
    /// <summary>
    /// Interaction logic for ResourceSection.xaml
    /// </summary>
    public partial class ResourceSection : UserControl
    {
        public ResourceSection()
        {
            InitializeComponent();
        }

        FactoryVM FactoryVm { get { return (FactoryVM)this.DataContext; } }

        private void DataGrid_KeyDown(object sender, KeyEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            FactoryVM factoryVm = (FactoryVM)dg.DataContext;

            switch (e.Key)
            {
            case Key.Escape:
                SetClosed();
                dg.SelectedIndex = -1;
                break;

            case Key.C:
                FactoryVm.ConvertSelectedRowToProduction(FactoryVm.GameDataVm);
                break;

            case Key.O:
                SetOpenClose();
                break;
            }
        }

        private void ResourceRowButtonClick(object sender, RoutedEventArgs e)
        {

        }

        private void AddColumnAsResource(object sender, RoutedEventArgs e)
        {

        }

        private void ConvertRowToProduction_Handler(object sender, RoutedEventArgs e)
        {
            var row = (FactoryRowVM)((FrameworkElement)sender).DataContext;
            FactoryVM factoryVm = FactoryVm;
            factoryVm.ConvertResourceRowToProduction(row, FactoryVm.GameDataVm);
        }

        private void OpenCloseRow_Handler(object sender, RoutedEventArgs e)
        {
            SetOpenClose();
        }

        private void SetOpenClose()
        {
            if (FactoryVm.RowDetailsVisibilityMode == DataGridRowDetailsVisibilityMode.VisibleWhenSelected)
                FactoryVm.RowDetailsVisibilityMode = DataGridRowDetailsVisibilityMode.Collapsed;
            else
                FactoryVm.RowDetailsVisibilityMode = DataGridRowDetailsVisibilityMode.VisibleWhenSelected;
        }

        private void SetClosed()
        {
            if (FactoryVm.RowDetailsVisibilityMode == DataGridRowDetailsVisibilityMode.VisibleWhenSelected)
                FactoryVm.RowDetailsVisibilityMode = DataGridRowDetailsVisibilityMode.Collapsed;
        }
    }
}
