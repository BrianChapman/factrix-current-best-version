﻿using Factrix.ViewModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Factrix.Xaml
{
    /// <summary>
    /// Interaction logic for ProductionSection.xaml
    /// </summary>
    public partial class ProductionSection : UserControl
    {
        public ProductionSection()
        {
            InitializeComponent();
            object o = DataContext;
        }

        FactoryVM FactoryVm {  get { return (FactoryVM)this.DataContext; } }

        private void DataGrid_KeyDown(object sender, KeyEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            FactoryVM factoryVm = (FactoryVM)dg.DataContext;

            switch (e.Key)
            {
            case Key.Escape:
                SetClosed();
                dg.SelectedIndex = -1;
                break;

            case Key.C:
                FactoryVm.ConvertSelectedRowToResource(FactoryVm.GameDataVm);
                break;

            case Key.O:
                SetOpenClose();
                break;
            }
        }

        //object _previousSelection;

        private void ProductionRowButtonClick(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("Button click");
        }

        private void ConvertRowToResourceHandler(object sender, RoutedEventArgs e)
        {
            var row = (FactoryRowVM)((FrameworkElement)sender).DataContext;
            FactoryVM factoryVm = FactoryVm;
            factoryVm.ConvertProductionRowToResource(row, FactoryVm.GameDataVm);
        }

        private void OpenCloseRowHandler(object sender, RoutedEventArgs e)
        {
            SetOpenClose();
        }

        private void SetOpenClose()
        {
            if (FactoryVm.RowDetailsVisibilityMode == DataGridRowDetailsVisibilityMode.VisibleWhenSelected)
                FactoryVm.RowDetailsVisibilityMode = DataGridRowDetailsVisibilityMode.Collapsed;
            else
                FactoryVm.RowDetailsVisibilityMode = DataGridRowDetailsVisibilityMode.VisibleWhenSelected;
        }

        private void SetClosed()
        {
            if (FactoryVm.RowDetailsVisibilityMode == DataGridRowDetailsVisibilityMode.VisibleWhenSelected)
                FactoryVm.RowDetailsVisibilityMode = DataGridRowDetailsVisibilityMode.Collapsed;
        }

    }
}
