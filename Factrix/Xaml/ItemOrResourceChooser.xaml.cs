﻿using FactorioDataReader;
using Factrix.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Factrix
{
    public class ChooserDataContext<T>: INotifyPropertyChanged
    {
        List<MenuGroup<T>> _categories;
        public List<MenuGroup<T>> Categories
        {
            get { return _categories; }
            set
            {
                _categories = value;
                Notify("Categories");
            }
        }

        MenuGroup<T> _selectedCategory;
        public MenuGroup<T> SelectedCategory
        {
            get { return _selectedCategory; }
            set
            {
                _selectedCategory = value;
                SelectedThing = null;
                CurrentSubSection = _selectedCategory.Group;
            }
        }

        List<MenuGroup<T>> _currentSubSection;
        public List<MenuGroup<T>> CurrentSubSection
        {
            get { return _currentSubSection; }
            set
            {
                _currentSubSection = value;
                Notify("CurrentSubSection");
            }
        }

        List<MenuGroup<T>> _currentSubSectionList;
        public List<MenuGroup<T>> CurrentSubSectionList
        {
            get { return _currentSubSectionList; }
            set
            {
                _currentSubSectionList = value;
                Notify("CurrentSubSectionList");
            }
        }

        public MenuGroup<T> SelectedThing { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        void Notify(string prop)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(prop));
        }
    }
    /// <summary>
    /// Interaction logic for ItemResourceChooser.xaml
    /// </summary>
    public partial class ItemOrResourceChooser : Window
    {
        ChooserDataContext<Item> _itemsContext;
        ChooserDataContext<Recipe> _recipesContext;

        public Item SelectedItem { get; set; }
        public Recipe SelectedRecipe { get; set; }

        public ItemOrResourceChooser(List<MenuGroup<Item>> menuGroups)
        {
            InitializeComponent();
            Title = "Resource Chooser";
            _itemsContext = new ChooserDataContext<Item>();
            _itemsContext.Categories = menuGroups;

            DataContext = _itemsContext;
        }

        public ItemOrResourceChooser(List<MenuGroup<Recipe>> recipesMenuGroups)
        {
            InitializeComponent();
            Title = "Production Chooser";
            _recipesContext = new ChooserDataContext<Recipe>();
            _recipesContext.Categories = recipesMenuGroups;

            DataContext = _recipesContext;
        }

        private void OkClick(object sender, RoutedEventArgs e)
        {
            if (_itemsContext != null && _itemsContext.SelectedThing != null)
            {
                SelectedItem = _itemsContext.SelectedThing.Thing;
                DialogResult = true;
            }
            if (_recipesContext != null && _recipesContext.SelectedThing != null)
            {
                SelectedRecipe = _recipesContext.SelectedThing.Thing;
                DialogResult = true;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            FrameworkElement fe = (FrameworkElement)sender;
            Object dataContext = fe.DataContext;
            MenuGroup<Recipe> recipeMenu = (MenuGroup<Recipe>)dataContext;
            SelectedRecipe = recipeMenu.Thing;
            DialogResult = true;
        }
    }
}
