﻿using FactorioDataReader;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Factrix.ViewModel
{
    public class FactoryVM: INotifyPropertyChanged
    {
        public ObservableCollection<FactoryRowVM> ResourceRowsVm { get; private set; }
        public ObservableCollection<FactoryRowVM> ProductionRowsVm { get; private set; }

        public ObservableCollection<Item> ActiveItems { get; private set; }

        public TotalsRowVM TotalsRow { get; private set; }
        public GameDataVM GameDataVm { get; set; }

        public FactoryVM()
        {
            ResourceRowsVm = new ObservableCollection<FactoryRowVM>();
            ProductionRowsVm = new ObservableCollection<FactoryRowVM>();
            TotalsRow = new TotalsRowVM(this);

            ResourceRowsVm.CollectionChanged += RecalculateEventHandler;
            ProductionRowsVm.CollectionChanged += RecalculateEventHandler;

            ActiveItems = new ObservableCollection<Item>();
            RowDetailsVisibilityMode = DataGridRowDetailsVisibilityMode.Collapsed;
        }

        DataGridRowDetailsVisibilityMode _rowDetailsVisibilityMode;
        public DataGridRowDetailsVisibilityMode RowDetailsVisibilityMode
        {
            get { return _rowDetailsVisibilityMode; }
            set
            {
                _rowDetailsVisibilityMode = value;
                Notify("RowDetailsVisibilityMode");
            }
        }

        private FactoryRowVM _selectedItemRow;
        public FactoryRowVM SelectedRow
        {
            get { return _selectedItemRow; }
            set
            {
                _selectedItemRow = value;
                Notify("SelectedRow");
                Notify("IsARowSelected");
            }
        }

        public bool IsARowSelected { get { return SelectedRow != null; } }

        public ResourceRowVM AddResource(Item item)
        {
            var row = new ResourceRowVM(item, this);
            ResourceRowsVm.Add(row);
            row.PropertyChanged += RecalculateEventHandler;
            Recalculate();
            return row;
        }

        public ProductionRowVM AddProduction(Recipe recipe)
        {
            var row = new ProductionRowVM(recipe, this);
            ProductionRowsVm.Add(row);
            row.PropertyChanged += RecalculateEventHandler;
            Recalculate();
            return row;
        }

        public void MoveSelectedRowUp()
        {
            var row = SelectedRow;
            MoveRowUp(row);
            SelectedRow = row;
            SortHeaders();
        }

        public void MoveSelectedRowDown()
        {
            var row = SelectedRow;
            MoveRowDown(row);
            SelectedRow = row;
            SortHeaders();
        }

        public void AddProductionTree(Recipe recipe, GameDataVM gameDataVm)
        {
            if (IsProductionRedundant(recipe))
                return;

            AddProduction(recipe);
            foreach(var itemResource in recipe.Ingredients)
            {
                Item item = itemResource.Item;
                if (item.SubgroupName == "fluid")
                {
                    if(!IsItemRedundant(item))
                        AddResource(item);
                }
                else
                {
                    Recipe subRecipe = gameDataVm.FindRecipeForItem(item);
                    if (subRecipe == null)
                    {
                        if(!IsItemRedundant(item))
                            AddResource(item);
                    }
                    else
                        AddProductionTree(subRecipe, gameDataVm);
                }
            }
            SortHeaders();
        }

        public void ConvertSelectedRowToResource(GameDataVM gameDataVm)
        {
            if (SelectedRow == null)
                return;
            FactoryRowVM row = SelectedRow;
            var newRow = ConvertProductionRowToResource(row, gameDataVm);
            SelectedRow = newRow;
            SortHeaders();
        }

        public void ConvertSelectedRowToProduction(GameDataVM gameDataVm)
        {
            if (SelectedRow == null)
                return;
            FactoryRowVM row = SelectedRow;
            var newRow = ConvertResourceRowToProduction(row, gameDataVm);
            SelectedRow = newRow;
            SortHeaders();
        }

        public FactoryRowVM ConvertProductionRowToResource(FactoryRowVM row, GameDataVM gameDataVm)
        {
            FactoryRowVM newRow = null;
            if (row is ProductionRowVM)
            {
                Recipe recipe = ((ProductionRowVM)row).Recipe;
                if (recipe.Results.Count > 1)
                    return null;
                Item item = recipe.Results[0].Item;
                newRow = AddResource(item);
                if (recipe.Results.Count == 1)
                    RemoveRow(row);
                RemoveUnneededIngredientResources(item);
            }
            SortHeaders();
            return newRow;
        }

        public FactoryRowVM ConvertResourceRowToProduction(FactoryRowVM row, GameDataVM gameDataVm)
        {
            FactoryRowVM newRow = null;
            if (row is ResourceRowVM)
            {
                Item item = ((ResourceRowVM)row).Item;
                Recipe recipe = gameDataVm.FindRecipeForItem(item);
                if (recipe == null)
                    return null;
                RemoveRow(row);
                AddProductionTree(recipe, gameDataVm);
            }
            SortHeaders();
            return newRow;
        }

        public void GetHeaderIndexRangeForResources(out int start, out int end)
        {
            start = ProductionRowsVm.Count;
            end = start + ResourceRowsVm.Count;
        }

        // ========== private =========

        private void RecalculateEventHandler(object sender, EventArgs e)
        {
            Recalculate();
        }

        private void RemoveRow(FactoryRowVM row)
        {
            if (row is ProductionRowVM)
                ProductionRowsVm.Remove(row);
            else if (row is ResourceRowVM)
                ResourceRowsVm.Remove(row);
        }

        private void Recalculate()
        {
            RecalculateHeaders();
            RecalculateValues();
        }


        // Update the total Items Columns
        private void RecalculateHeaders()
        {
            // Collect a Set of all the Items that is
            // A union of all the Items in each row.
            HashSet<Item> _itemSet = new HashSet<Item>();
            foreach(var row in ProductionRowsVm)
            {
                var prodRow = row as ProductionRowVM;
                if (prodRow != null)
                {
                    foreach (var itemResource in prodRow.Recipe.Results)
                        _itemSet.Add(itemResource.Item);
                }
            }
            foreach(var row in ResourceRowsVm)
            {
                var resRow = row as ResourceRowVM;
                if(resRow != null)
                {
                    _itemSet.Add(resRow.Item);
                }
            }

            // If anything is in the master "ActiveList"
            // That should not be be there, remove it.
            List<Item> removeList = new List<Item>();
            foreach(Item item in ActiveItems)
            {
                if (!_itemSet.Contains(item))
                    removeList.Add(item);
            }
            foreach (Item item in removeList)
                ActiveItems.Remove(item);

            // If any thing that should be there isn't Add it.
            foreach (Item item in _itemSet)
            {
                if (!ActiveItems.Contains(item))
                {
                    ActiveItems.Add(item);
                }
            }
        }

        private void RecalculateValues()
        {
            List<Item> activeItems = new List<Item>(ActiveItems);
            foreach (var row in ResourceRowsVm)
                row.FactoryItemsList = activeItems;
            foreach (var row in ProductionRowsVm)
                row.FactoryItemsList = activeItems;
            TotalsRow.FactoryItemsList = activeItems;
            TotalsRow.UpdateFactoryItemRates();
        }

        private void SortHeaders()
        {
            List<Item> productionList = new List<Item>();
            List<Item> resourceList = new List<Item>();
            foreach (var row in ProductionRowsVm)
            {
                var prodRow = row as ProductionRowVM;
                if (prodRow == null)
                    throw new Exception("bad row in ProducitonRowsVm");
                foreach (var resource in prodRow.Recipe.Results)
                {
                    //ActiveItems.Remove(resource.Item);
                    if(!productionList.Contains(resource.Item))
                        productionList.Add(resource.Item);
                }
            }
            foreach (var row in ResourceRowsVm)
            {
                var resRow = row as ResourceRowVM;
                if (resRow == null)
                    throw new Exception("bad row in ResourceRowsVm");
                var item = resRow.Item;
                //ActiveItems.Remove(item);
                if(!resourceList.Contains(item))
                    resourceList.Add(item);
            }
            ActiveItems.Clear();
            foreach (var item in productionList)
                ActiveItems.Add(item);
            foreach (var item in resourceList)
                ActiveItems.Add(item);
            RecalculateValues();
        }

        private void MoveRowUp(FactoryRowVM row)
        {
            if (row == null)
                return;
            ObservableCollection<FactoryRowVM> collection = GetRowsCollection(row);
            if (row == null)
                return;
            int index = collection.IndexOf(row);
            if (index < 0)  // -1 not present
                return;

            if (index < 1)  // 0 already first
                return;
            collection.RemoveAt(index);
            collection.Insert(index - 1, row);
        }

        private void MoveRowDown(FactoryRowVM row)
        {
            if (row == null)
                return;
            ObservableCollection<FactoryRowVM> collection = GetRowsCollection(row);
            if (row == null)
                return;
            int index = collection.IndexOf(row);
            if (index < 0)  // -1 not present
                return;

            if (index > collection.Count - 2)  // already last
                return;
            collection.RemoveAt(index);
            if (index == collection.Count - 2)
                collection.Add(row);
            else
                collection.Insert(index + 1, row);
        }

        private ObservableCollection<FactoryRowVM> GetRowsCollection(FactoryRowVM row)
        {
            if (row is ResourceRowVM)
                return ResourceRowsVm;
            else if (row is ProductionRowVM)
                return ProductionRowsVm;
            return null;
        }

        private bool IsProductionRedundant(Recipe recipe)
        {
            foreach (var row in ProductionRowsVm)
            {
                var prodRow = (ProductionRowVM)row;
                if (prodRow.Recipe == recipe)
                    return true;
            }
            return false;
        }

        private bool IsItemRedundant(Item item)
        {
            List<Item> items = new List<Item>();
            items.Add(item);
            return 0 == DeleteRedundantItems(items);
        }

        private int DeleteRedundantItems(List<Item> items)
        {
            foreach (var row in ProductionRowsVm)
            {
                var prodRow = (ProductionRowVM)row;
                foreach (var itemResource in prodRow.Recipe.Results)
                    items.Remove(itemResource.Item);
            }
            foreach (var row in ResourceRowsVm)
            {
                var resRow = (ResourceRowVM)row;
                items.Remove(resRow.Item);
            }
            return items.Count;
        }

        private void RemoveUnneededIngredientResources(Item item)
        {
            List<ResourceRowVM> deleteRows = new List<ResourceRowVM>();
            foreach(ResourceRowVM rrow in ResourceRowsVm)
            {
                int index = ActiveItems.IndexOf(rrow.Item);
                bool used = false;
                foreach(ProductionRowVM prow in ProductionRowsVm)
                {
                    if(prow.FactoryItemRates[index].Actual != 0)
                    {
                        used = true;
                        break;
                    }
                }
                if (!used)
                    deleteRows.Add(rrow);
            }
            foreach (var row in deleteRows)
                RemoveRow(row);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void Notify(string prop)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(prop));
        }

    }
}
