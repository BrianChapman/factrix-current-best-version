﻿using FactorioDataReader;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix.ViewModel
{
    public class MasterVM
    {
        public GameDataVM GameDataVm { get; private set; }
        public FactoryVM FactoryVm { get; private set; }
        public List<MenuGroup<Recipe>> RecipeMenuGroups { get; private set; }
        public List<MenuGroup<Item>> ItemMenuGroups { get; private set; }
        public Dictionary<String, List<Recipe>> RecipeCategories { get; private set; }

        public MasterVM()
        {
            GameDataVm = new GameDataVM();
            FactoryVm = new FactoryVM();
            FactoryVm.GameDataVm = GameDataVm;

            ItemMenuGroups = HierarchicalGroupVM.BuildMenuGroups<Item>(GameDataVm.ItemTableVm, GameDataVm.ItemGroupVm);
            RecipeMenuGroups = HierarchicalGroupVM.BuildMenuGroups<Recipe>(GameDataVm.RecipeTableVm, GameDataVm.ItemGroupVm);
        }

    }
}
