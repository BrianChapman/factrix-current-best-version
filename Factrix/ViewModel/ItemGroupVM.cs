﻿using FactorioDataReader;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix.ViewModel
{
    public class ItemGroupTableVM : List<ItemGroup>
    {
        static public ItemGroupTableVM LoadItemGroups(Dictionary<string, ItemGroup> itemGroups)
        {
            var table = new ItemGroupTableVM();

            foreach (var group in itemGroups.Values)
                table.Add(group);
            return table;
        }
    }

    public class ItemSubgroupVM : List<ItemSubgroup>
    {
        static public ItemSubgroupVM LoadItemGroups(Dictionary<string, ItemSubgroup> itemSubgroups)
        {
            var table = new ItemSubgroupVM();

            foreach (var subgroup in itemSubgroups.Values)
                table.Add(subgroup);
            return table;
        }
    }

}
