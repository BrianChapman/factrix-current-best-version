﻿using FactorioDataReader;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix.ViewModel
{
    public class MenuGroup<T>
    {
        public string Name { get; private set; }
        public string FriendlyName { get; private set; }
        public string IconPath { get; private set; }
        public T Thing { get; private set; }
        public List<MenuGroup<T>> Group { get; private set; }

        public MenuGroup(string name, string friendlyName, string iconPath, T thing, List<MenuGroup<T>> group)
        {
            Name = name;
            FriendlyName = friendlyName;
            IconPath = iconPath;
            Thing = thing;
            Group = group;
        }

        public bool HasContent
        {
            get
            {
                // Leaf nodes are always Content.
                if (Group == null)
                    return true;

                // IF you have any children that have any children
                // that have content then you have content.
                foreach(var subgroup in Group)
                {
                    if (subgroup.HasContent)
                        return true;
                }
                return false;
            }
        }

    }

    static class HierarchicalGroupVM
    {
        static public List<MenuGroup<T>> BuildMenuGroups<T>(IEnumerable<T> things, ItemGroupTableVM itemGroups) where T : class
        {
            var subgroupTable = new Dictionary<string, List<MenuGroup<T>>>();
            var groupList = new List<MenuGroup<T>>();
            List<MenuGroup<T>> unsortedGroup = null;
            foreach (var group in itemGroups)
            {
                var menuGroup = new List<MenuGroup<T>>();
                groupList.Add(new MenuGroup<T>(group.Name, group.FriendlyName, group.IconPath, null, menuGroup));
                if (group.Name == "other")
                    unsortedGroup = menuGroup;
                foreach(var subgroup in group.Subgroups)
                {
                    var menuSubgroup = new List<MenuGroup<T>>();
                    menuGroup.Add(new MenuGroup<T>(subgroup.Name, subgroup.Name, null, null, menuSubgroup));
                    subgroupTable.Add(subgroup.Name, menuSubgroup);
                }
            }
            foreach(T thing in things)
            {
                Recipe recipe = thing as Recipe;
                Item item = thing as Item;
                string subgroupName = (recipe != null) ? recipe.Category : item.SubgroupName;
                string thingName = (recipe != null) ? recipe.Name : item.Name;
                string friendlyName = (recipe != null) ? recipe.FriendlyName : item.FriendlyName;
                string iconPath = (recipe != null) ? recipe.IconPath : item.IconPath;

                List<MenuGroup<T>> subgroupList = null;
                if(!subgroupTable.TryGetValue(subgroupName, out subgroupList))
                {
                    subgroupList = new List<MenuGroup<T>>();
                    unsortedGroup.Add(new MenuGroup<T>(subgroupName, subgroupName, null, null, subgroupList));
                    subgroupTable.Add(subgroupName, subgroupList);
                }
                subgroupList.Add(new MenuGroup<T>(thingName, friendlyName, iconPath, thing, null));
            }
            return groupList;
        }

    }
}
