﻿using FactorioDataReader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix.ViewModel
{
    public class GameDataVM
    {
        public ItemTableVM ItemTableVm { get; private set; }
        public RecipeTableVM RecipeTableVm { get; private set; }
        public ItemGroupTableVM ItemGroupVm { get; private set; }

        public GameDataVM()
        {
            GameData gameData = new GameData();
            gameData.Difficulty = Difficulty.Normal;
            gameData.EnabledMods = null;
            gameData.Locale = "en";

            ItemTableVm = ItemTableVM.LoadItemCollection(gameData.Items);
            RecipeTableVm = RecipeTableVM.LoadRecipeCollection(gameData.Recipes);
            ItemGroupVm = ItemGroupTableVM.LoadItemGroups(gameData.ItemGroups);
        }

        public Recipe FindRecipeForItem(Item item)
        {
            List<Recipe> recipeList = new List<Recipe>();
            foreach (Recipe recipe in RecipeTableVm)
            {
                foreach(ItemResource result in recipe.Results)
                {
                    if (result.Item == item)
                        recipeList.Add(recipe);
                }
            }

            if (recipeList.Count == 0)
                return null;

            // If there is only one recipe return it (the common case)
            if (recipeList.Count == 1)
                return recipeList[0];

            // If there is more than one recipe, return the one with
            // the single result.
            foreach(Recipe recipe in recipeList)
            {
                if (recipe.Results.Count == 1)
                    return recipe;
            }
            
            // Otherwise just return a recipe at random, the first one.
            return recipeList[0];
        }
    }
}