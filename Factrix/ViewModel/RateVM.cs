﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix.ViewModel
{
    [DebuggerDisplay("{Display}")]
    public class RateVM: INotifyPropertyChanged 
    {
        public RateVM(float val)
        {
            Actual = val;
            Display = Format(val);
        }

        string _display;
        public string Display
        {
            get { return _display; }
            set
            {
                if (_display == value)
                    return;

                _display = value;

                float actual;
                if (float.TryParse(_display, out actual))
                {
                    Actual = actual;
                }
                Notify("Display");
            }
        }

        float _actual;

        public float Actual
        {
            get { return _actual; }
            set
            {
                if (_actual == value)
                    return;
                _actual = value;
                Display = Format(_actual);
                Notify("Actual");
            }
        }

        #region Optimization
        public void SetActual(float val)
        {
            _actual = val;      // bypass the Display and the Notify.
        }

        public void FinishActual()
        {
            Display = Format(_actual);
            Notify("Actual");
        }
        #endregion

        public static string Format(float val)
        {
            if (Math.Abs(val) < 0.0001)
                return String.Empty;
            if(Math.Abs(val) < 1)
                return String.Format("{0:0.000}", val);
            if(Math.Abs(val) < 10)
                return String.Format("{0:0.00}", val);
            if(Math.Abs(val) < 100)
                return String.Format("{0:0.0}", val);
            return String.Format("{0}", (int)Math.Round(val));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void Notify(string prop)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(prop));
        }

    }
}
