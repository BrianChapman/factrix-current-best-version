﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Factrix.ViewModel
{
    public class BoolToSomethingConverter<T> : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo language)
        {
            bool boolValue = false;
            if (value != null)
            {
                boolValue = (bool)value;
            }
            var p = (BoolToSomethingParameter<T>)parameter;
            if (p == null)
            {
                return default(T);
            }
            return (boolValue) ? p.OnTrue : p.OnFalse;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class BoolToSomethingParameter<T>
    {
        public T OnTrue { get; set; }
        public T OnFalse { get; set; }
    }

    public class BoolToDoubleConverter : BoolToSomethingConverter<Double> { }
    public class BoolToDoubleParameter : BoolToSomethingParameter<Double> { }

    public class BoolToBrushConverter : BoolToSomethingConverter<Brush> { }
    public class BoolToBrushParameter : BoolToSomethingParameter<Brush> { }

    public class BoolToVisibilityConverter : BoolToSomethingConverter<Visibility> { }
    public class BoolToVisibilityParameter : BoolToSomethingParameter<Visibility> { }

}
