﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Factrix.ViewModel
{
    public class FloatToSomethingConverter<T> : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo language)
        {
            float floatValue = 0;
            if (value != null)
            {
                floatValue = (float)value;
            }
            var p = (FloatToSomethingParameter<T>)parameter;
            if (p == null)
            {
                return default(T);
            }
            if (floatValue > p.ZeroEpsilon)
                return p.Positive;
            if (floatValue < -p.ZeroEpsilon)
                return p.Negative;
            return p.Zero;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class FloatToSomethingParameter<T>
    {
        public T Positive { get; set; }
        public T Negative { get; set; }
        public T Zero { get; set; }
        public float ZeroEpsilon { get; set; }
    }

    public class FloatToBrushConverter : FloatToSomethingConverter<Brush> { }
    public class FloatToBrushParameter : FloatToSomethingParameter<Brush> { }


}
