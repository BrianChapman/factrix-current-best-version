﻿using FactorioDataReader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Factrix.ViewModel
{
    public class TotalsRowVM : FactoryRowVM
    {
        public TotalsRowVM(FactoryVM factoryVm)
            :base(factoryVm)
        {
        }

        public override void UpdateFactoryItemRates()
        {
            if (FactoryItemsList == null || FactoryItemsList.Count == 0)
                return;

            List<RateVM> totalRates = new List<RateVM>();
            for (int i = 0; i < FactoryItemsList.Count; ++i)
                totalRates.Add(new RateVM(0));

            SumRows(totalRates, Parent.ResourceRowsVm);
            SumRows(totalRates, Parent.ProductionRowsVm);

            FactoryItemRates = totalRates;
        }

        private void SumRows(List<RateVM> sumList, IEnumerable<FactoryRowVM> rows)
        {
            foreach (var row in rows)
            {
                List<RateVM> rates = row.FactoryItemRates;
                for (int i = 0; i < rates.Count; ++i)
                    sumList[i].Actual += rates[i].Actual;
            }
        }
    }
}
