﻿using FactorioDataReader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix.ViewModel
{
    public class ResourceRowVM: FactoryRowVM
    {
        public ResourceRowVM(Item item, FactoryVM parent)
            : base(parent)
        {
            Item = item;
            Rate = new RateVM(1);
            Rate.PropertyChanged += Recalc;
        }

        public Item Item { get; private set; }
        public RateVM Rate { get; private set; }

        public override void UpdateFactoryItemRates()
        {
            if (FactoryItemsList == null || FactoryItemsList.Count == 0)
                return;

            List<RateVM> deliveryRates = new List<RateVM>();

            int itemHeaderStartIndex = Parent.ProductionRowsVm.Count;
            int i = 0;
            foreach(Item item in FactoryItemsList)
            {
                if (i < itemHeaderStartIndex)
                    deliveryRates.Add(new RateVM(0));
                else
                {
                    if (item == Item)
                        deliveryRates.Add(new RateVM(Rate.Actual));
                    else
                        deliveryRates.Add(new RateVM(0));
                }
                i += 1;
            }
            FactoryItemRates = deliveryRates;
        }

        private void Recalc(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            UpdateFactoryItemRates();
        }


    }
}
