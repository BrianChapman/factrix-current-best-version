﻿using FactorioDataReader;
using System.Collections.Generic;
using System.Diagnostics;

namespace Factrix.ViewModel
{
    [DebuggerDisplay("{Recipe.Name}")]
    public class ProductionRowVM: FactoryRowVM
    {
        public Recipe Recipe { get; private set; }

        Dictionary<Item, float> _localProduction;

        int _units;
        public int Units
        {
            get { return _units; }
            set
            {
                _units = value;
                Notify("Units");
                UpdateFactoryItemRates();
            }
        }

        public ProductionRowVM(Recipe recipe, FactoryVM parent)
            : base(parent)
        {
            Recipe = recipe;
            _localProduction = new Dictionary<Item, float>();
            Units = 1;

            foreach (ItemResource itemRes in Recipe.Results)
                _localProduction[itemRes.Item] = RateFactor * itemRes.Amount / Recipe.BaseTime;
            foreach (ItemResource itemRes in Recipe.Ingredients)
                _localProduction[itemRes.Item] = -RateFactor * itemRes.Amount / Recipe.BaseTime;
        }

        private float RateFactor
        {
            get { return 1.0f; }   // computed based on the Assembler used and modules present, etc.
        }

        public override void UpdateFactoryItemRates()
        {
            if (FactoryItemsList == null || FactoryItemsList.Count == 0)
                return;

            List<RateVM> factoryRates = new List<RateVM>();
            foreach(Item item in FactoryItemsList)
            {
                float rate;
                if (_localProduction.TryGetValue(item, out rate))
                    factoryRates.Add(new RateVM(rate * Units));
                else
                    factoryRates.Add(new RateVM(0));
            }
            FactoryItemRates = factoryRates;
        }

    }
}
