﻿using FactorioDataReader;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix.ViewModel
{
    abstract public class FactoryRowVM : INotifyPropertyChanged
    {
        protected FactoryVM Parent { get; private set; }

        protected FactoryRowVM(FactoryVM parent)
        {
            Parent = parent;
        }

        List<Item> _factoryItemList;
        public List<Item> FactoryItemsList
        {
            get { return _factoryItemList; }
            set
            {
                if (!Same(_factoryItemList, value))
                {
                    _factoryItemList = value;
                    UpdateFactoryItemRates();
                }
            }
        }

        List<RateVM> _factoryItemRates;
        public List<RateVM> FactoryItemRates
        {
            get { return _factoryItemRates; }
            set
            {
                _factoryItemRates = value;
                Notify("FactoryItemRates");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void Notify(string prop)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(prop));
        }

        abstract public void UpdateFactoryItemRates();


        private bool Same(List<Item> a, List<Item> b)
        {
            if ((a == null || b == null) && a != b)
                return false;

            if (a.Count != b.Count)
                return false;

            for(int i=0; i<a.Count; ++i)
            {
                if (a[i] != b[i])
                    return false ;
            }

            return true;  // the same
        }
    }

}
