﻿using FactorioDataReader;
using Factrix.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Factrix
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        object _previousSelection;

        private void ProductionRowButtonClick(object sender, RoutedEventArgs e)
        {
            var factoryVm = GetFactory((FrameworkElement)sender);

            if (_previousSelection == sender)
            {
                factoryVm.SelectedRow = null;
                _previousSelection = null;
                factoryVm.RowDetailsVisibilityMode = DataGridRowDetailsVisibilityMode.Collapsed;
            }
            else
            {
                _previousSelection = sender;
                factoryVm.RowDetailsVisibilityMode = DataGridRowDetailsVisibilityMode.VisibleWhenSelected;
            }

        }

        private void ResourceRowButtonClick(object sender, RoutedEventArgs e)
        {

        }

        private void Menu_AddProduction(object sender, RoutedEventArgs e)
        {
            var menuItem = (MenuItem)e.OriginalSource;
            var group = (MenuGroup<Recipe>)menuItem.Header;

            _masterVm.FactoryVm.AddProduction(group.Thing);
        }

        private void Menu_AddResource(object sender, RoutedEventArgs e)
        {
            var menuItem = (MenuItem)e.OriginalSource;
            var group = (MenuGroup<Item>)menuItem.Header;

            _masterVm.FactoryVm.AddResource(group.Thing);
        }

        private void OpenRecipeChooser(object sender, RoutedEventArgs e)
        {
            var fe = (FrameworkElement)sender;
            var masterVm = (MasterVM)fe.DataContext;
            var factoryVm = masterVm.FactoryVm;

            var resourceChooser = new ItemOrResourceChooser(_masterVm.RecipeMenuGroups);
            bool? isOK = resourceChooser.ShowDialog();
            if (isOK == null || !isOK.Value)
                return;

            factoryVm.AddProductionTree(resourceChooser.SelectedRecipe, _masterVm.GameDataVm);
        }

        private void OpenItemChooser(object sender, RoutedEventArgs e)
        {
            var resourceChooser = new ItemOrResourceChooser(_masterVm.ItemMenuGroups);
            bool? isOK = resourceChooser.ShowDialog();

        }

        private void Click_MoveRowUp(object sender, RoutedEventArgs e)
        {
            var factoryVm = GetFactory((FrameworkElement)sender);
            factoryVm.MoveSelectedRowUp();
        }

        private void Click_MoveRowDown(object sender, RoutedEventArgs e)
        {
            var factoryVm = GetFactory((FrameworkElement)sender);
            factoryVm.MoveSelectedRowDown();
        }

        private FactoryVM GetFactory(FrameworkElement sender)
        {
            var factoryVm = _masterVm.FactoryVm;
            return factoryVm;
        }

        private void AddColumnAsResource(object sender, RoutedEventArgs e)
        {
            var fe = (FrameworkElement)sender;
            var item = (Item)fe.DataContext;
            _masterVm.FactoryVm.AddResource(item);
        }

        private void AddColumnAsProduction(object sender, RoutedEventArgs e)
        {
            var fe = (FrameworkElement)sender;
            var item = (Item)fe.DataContext;
            var recipe = _masterVm.GameDataVm.FindRecipeForItem(item);
            if(recipe != null)
                _masterVm.FactoryVm.AddProduction(recipe);
        }

        private void AddRecipeIngredientsAsProductions(object sender, RoutedEventArgs e)
        {
            var row = (ProductionRowVM)((FrameworkElement)sender).DataContext;
            var recipe = row.Recipe;
            foreach (var resource in recipe.Ingredients)
            {
                Recipe production = _masterVm.GameDataVm.FindRecipeForItem(resource.Item);
                if(production != null)
                    _masterVm.FactoryVm.AddProductionTree(production, _masterVm.GameDataVm);
            }
        }

        //private void ConvertRow(object sender, RoutedEventArgs e)
        //{
        //    var row = (FactoryRowVM)((FrameworkElement)sender).DataContext;
        //    FactoryVM factoryVm = _masterVm.FactoryVm;
        //    factoryVm.ConvertRow(row, _masterVm.GameDataVm);
        //}


        //private void Context_DeleteRow(object sender, RoutedEventArgs e)
        //{
        //    var row = (FactoryRowVM)((FrameworkElement)sender).DataContext;
        //    var factoryVm = GetFactory((FrameworkElement)sender);
        //    factoryVm.DeleteRow(row);
        //}
    }
}
