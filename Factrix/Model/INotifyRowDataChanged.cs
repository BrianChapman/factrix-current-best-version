﻿using Factrix.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factrix.Model
{
    public class RowDataChangedEventArgs : EventArgs
    {
        public RowDataChangedEventArgs(FactoryRowVM factoryRow)
        {
            FactoryRow = factoryRow;
        }
        public virtual FactoryRowVM FactoryRow { get; }
    }


    public delegate void RowDataChangedEventHandler(object sender, RowDataChangedEventArgs e);


    public interface  INotifyRowDataChanged
    {
        event RowDataChangedEventHandler DataChanged;
    }
}
