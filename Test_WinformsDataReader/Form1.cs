﻿using FactorioDataReader;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test_WinformsDataReader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GameData gameData = ReadGameData();

            var items = gameData.Items;
            foreach (Item item in items.Values)
            {
                var itemCtrl = new ItemControl(item);
                itemCtrl.ItemSelected += ItemCtrl_ItemSelected;
                _flow.Controls.Add(itemCtrl);
            }
            _itemIconPictureBox.Size = new Size(128, 128);

            var recipes = gameData.Recipes;
            foreach(Recipe recipe in recipes.Values)
            {
                var recipeCtrl = new RecipeControl(recipe);
                recipeCtrl.ItemSelected += RecipeCtrl_ItemSelected;
                _recipeFlow.Controls.Add(recipeCtrl);
            }
        }

        private void ItemCtrl_ItemSelected(object sender, EventArgs e)
        {
            var itemCtrl = (ItemControl)sender;
            SetDetailView(itemCtrl);
        }

        private void RecipeCtrl_ItemSelected(object sender, EventArgs e)
        {
            var recipeCtrl = (RecipeControl)sender;
            SetRecipeDetailView(recipeCtrl);
        }

        private void SetDetailView(ItemControl itemCtrl)
        {
            Item item = itemCtrl.Item;
            _itemIconPictureBox.Image = ResizeBitmap(itemCtrl.Icon, _itemIconPictureBox.Width, _itemIconPictureBox.Height);
            _itemNameTextBox.Text = item.Name;
            _itemFriendlyNameTextBox.Text = item.FriendlyName;

            LoadListView(_itemCreatedByListBox, item.CreatedByRecipes);
            LoadListView(_itemUsedByListBox, item.UsedByRecipes);
            _itemCreatedByListBox.SelectedIndex = -1;
            _itemUsedByListBox.SelectedIndex = -1;
        }

        BindingSource _ingredientBindingSource = new BindingSource();
        BindingSource _resultBindingSource = new BindingSource();

        private void SetRecipeDetailView(RecipeControl recipeCtrl)
        {
            Recipe recipe = recipeCtrl.Recipe;
            _recipeIconPictureBox.Image = ResizeBitmap(recipeCtrl.Icon, _recipeIconPictureBox.Width, _recipeIconPictureBox.Height);
            _recipeNameTextBox.Text = recipe.Name;
            _recipeFriendlyNameTextBox.Text = recipe.FriendlyName;

            _resultBindingSource.Clear();
            _resultBindingSource.DataSource = typeof(Ingredient);
            foreach (var result in recipe.Results)
                _resultBindingSource.Add(new Ingredient(result));
            _resultsDataGridView.DataSource = _resultBindingSource;
            _resultsDataGridView.ClearSelection();

            _ingredientBindingSource.Clear();
            _ingredientBindingSource.DataSource = typeof(Ingredient);
            foreach (var ingredientResource in recipe.Ingredients)
                _ingredientBindingSource.Add(new Ingredient(ingredientResource));
            _ingredientDataGridView.DataSource = _ingredientBindingSource;
            _ingredientDataGridView.ClearSelection();
        }

        Bitmap ResizeBitmap(Bitmap bitmap, int width, int height)
        {
            Bitmap animage = new Bitmap(width, height);
            using (Graphics gr = Graphics.FromImage(animage))
            {
                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.InterpolationMode = InterpolationMode.NearestNeighbor;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.CompositingQuality = CompositingQuality.HighQuality;
                gr.DrawImage(bitmap, new Rectangle(0, 0, width, height));
            }
            return animage;
        }

        void LoadListView(ListBox lb, IEnumerable<Recipe> recipes)
        {
            List<string> recipeNames = new List<string>();

            foreach(var r in recipes)
            {
                recipeNames.Add(r.Name);
            }
            lb.Height = recipeNames.Count * lb.ItemHeight + lb.ItemHeight/2;
            lb.DataSource = recipeNames;
        }

        private GameData ReadGameData()
        {
            GameData gameData = new GameData();
            gameData.Difficulty = Difficulty.Normal;
            gameData.EnabledMods = null;
            gameData.Locale = "en";

            Dictionary<string, Item> items = gameData.Items;
            Dictionary<string, Recipe> recipes = gameData.Recipes;
            if (gameData.HasErrors)
            {
                foreach (var pair in gameData.FileErrors)
                {
                    Console.WriteLine("ERROR -- {0}: {1}", pair.Key, pair.Value.Message);
                }
            }

            return gameData;
        }
    }
}
