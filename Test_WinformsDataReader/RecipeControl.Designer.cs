﻿namespace Test_WinformsDataReader
{
    partial class RecipeControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._iconBox = new System.Windows.Forms.PictureBox();
            this._nameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this._iconBox)).BeginInit();
            this.SuspendLayout();
            // 
            // _iconBox
            // 
            this._iconBox.Location = new System.Drawing.Point(3, 3);
            this._iconBox.Name = "_iconBox";
            this._iconBox.Size = new System.Drawing.Size(115, 100);
            this._iconBox.TabIndex = 0;
            this._iconBox.TabStop = false;
            // 
            // _nameLabel
            // 
            this._nameLabel.AutoSize = true;
            this._nameLabel.Location = new System.Drawing.Point(153, 49);
            this._nameLabel.MaximumSize = new System.Drawing.Size(150, 0);
            this._nameLabel.MinimumSize = new System.Drawing.Size(150, 0);
            this._nameLabel.Name = "_nameLabel";
            this._nameLabel.Size = new System.Drawing.Size(150, 20);
            this._nameLabel.TabIndex = 1;
            this._nameLabel.Text = "recipe name";
            // 
            // RecipeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this._nameLabel);
            this.Controls.Add(this._iconBox);
            this.Name = "RecipeControl";
            this.Size = new System.Drawing.Size(454, 182);
            this.Click += new System.EventHandler(this.RecipeControl_Click);
            ((System.ComponentModel.ISupportInitialize)(this._iconBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox _iconBox;
        private System.Windows.Forms.Label _nameLabel;
    }
}
