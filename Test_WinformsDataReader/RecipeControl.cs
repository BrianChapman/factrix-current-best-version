﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FactorioDataReader;

namespace Test_WinformsDataReader
{
    public partial class RecipeControl : UserControl
    {
        Recipe _recipe;

        public RecipeControl(Recipe recipe)
        {
            InitializeComponent();

            _recipe = recipe;

            IconPath = _recipe.IconPath;
            _nameLabel.Text = _recipe.FriendlyName;

            _iconBox.Click += RecipeControl_Click;
            _nameLabel.Click += RecipeControl_Click;

        }

        public Recipe Recipe { get { return _recipe; } }
        public Bitmap Icon { get; private set; }

        string _iconPath;
        public string IconPath
        {
            get { return _iconPath; }
            set
            {
                _iconPath = value;
                Icon = new Bitmap(_iconPath);
                _iconBox.Image = Icon;
                _iconBox.Size = new Size(Icon.Width + 2, Icon.Height + 2);

                _nameLabel.Left = _iconBox.Left + _iconBox.Width + 5;
                _nameLabel.Top = _iconBox.Top + _iconBox.Height / 2 - _nameLabel.Height / 2;

                Width = _nameLabel.Right + 5;
                Height = _iconBox.Bottom + 5;
            }
        }

        public event EventHandler ItemSelected;

        private void RecipeControl_Click(object sender, EventArgs e)
        {
            var handler = ItemSelected;
            if (handler != null)
                handler(this, e);
        }


    }
}
