﻿using FactorioDataReader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_WinformsDataReader
{
    public class Ingredient
    {
        ItemResource _res;
        public Ingredient(ItemResource res)
        {
            _res = res;
        }

        public int Amount { get { return _res.Amount; } }
        public string ItemName { get { return _res.ItemName; } }
    }
}
