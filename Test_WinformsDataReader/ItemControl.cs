﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using FactorioDataReader;

namespace Test_WinformsDataReader
{
    public partial class ItemControl : UserControl
    {
        Item _item;

        public ItemControl(Item item)
        {
            InitializeComponent();

            _item = item;

            IconPath = _item.IconPath;
            _nameLabel.Text = _item.FriendlyName;

            _iconBox.Click += ItemControl_Click;
            _nameLabel.Click += ItemControl_Click;
        }

        public Item Item { get { return _item; } }
        public Bitmap Icon { get; private set; }

        string _iconPath;
        public string IconPath
        {
            get { return _iconPath; }
            set
            {
                _iconPath = value;
                Icon = new Bitmap(_iconPath);
                _iconBox.Image = Icon;
                _iconBox.Size = new Size(Icon.Width+2, Icon.Height+2);

                _nameLabel.Left = _iconBox.Left + _iconBox.Width + 5;
                _nameLabel.Top = _iconBox.Top + _iconBox.Height / 2 - _nameLabel.Height / 2;

                Width = _nameLabel.Right + 5;
                Height =  _iconBox.Bottom + 5;
            }
        }

        public event EventHandler ItemSelected;

        private void ItemControl_Click(object sender, EventArgs e)
        {
            var handler = ItemSelected;
            if (handler != null)
                handler(this, e);
        }
    }
}
