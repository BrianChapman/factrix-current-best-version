﻿namespace Test_WinformsDataReader
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this._flow = new System.Windows.Forms.FlowLayoutPanel();
            this._itemCreatedByListBox = new System.Windows.Forms.ListBox();
            this._createdByLabel = new System.Windows.Forms.Label();
            this._itemUsedByListBox = new System.Windows.Forms.ListBox();
            this._usedByLabel = new System.Windows.Forms.Label();
            this._itemFriendlyNameTextBox = new System.Windows.Forms.TextBox();
            this._itemNameTextBox = new System.Windows.Forms.TextBox();
            this._friendlyNameLabel = new System.Windows.Forms.Label();
            this._nameLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._itemIconPictureBox = new System.Windows.Forms.PictureBox();
            this._tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this._recipeFlow = new System.Windows.Forms.FlowLayoutPanel();
            this._resultsDataGridView = new System.Windows.Forms.DataGridView();
            this._ingredientDataGridView = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._recipeFriendlyNameTextBox = new System.Windows.Forms.TextBox();
            this._recipeNameTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._recipeIconPictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._itemIconPictureBox)).BeginInit();
            this._tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._resultsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._ingredientDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._recipeIconPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this._flow);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this._itemCreatedByListBox);
            this.splitContainer1.Panel2.Controls.Add(this._createdByLabel);
            this.splitContainer1.Panel2.Controls.Add(this._itemUsedByListBox);
            this.splitContainer1.Panel2.Controls.Add(this._usedByLabel);
            this.splitContainer1.Panel2.Controls.Add(this._itemFriendlyNameTextBox);
            this.splitContainer1.Panel2.Controls.Add(this._itemNameTextBox);
            this.splitContainer1.Panel2.Controls.Add(this._friendlyNameLabel);
            this.splitContainer1.Panel2.Controls.Add(this._nameLabel);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this._itemIconPictureBox);
            this.splitContainer1.Size = new System.Drawing.Size(1723, 1062);
            this.splitContainer1.SplitterDistance = 1210;
            this.splitContainer1.TabIndex = 0;
            // 
            // _flow
            // 
            this._flow.AutoScroll = true;
            this._flow.Dock = System.Windows.Forms.DockStyle.Fill;
            this._flow.Location = new System.Drawing.Point(0, 0);
            this._flow.Name = "_flow";
            this._flow.Size = new System.Drawing.Size(1210, 1062);
            this._flow.TabIndex = 0;
            // 
            // _itemCreatedByListBox
            // 
            this._itemCreatedByListBox.FormattingEnabled = true;
            this._itemCreatedByListBox.ItemHeight = 20;
            this._itemCreatedByListBox.Location = new System.Drawing.Point(36, 445);
            this._itemCreatedByListBox.MaximumSize = new System.Drawing.Size(380, 144);
            this._itemCreatedByListBox.MinimumSize = new System.Drawing.Size(380, 26);
            this._itemCreatedByListBox.Name = "_itemCreatedByListBox";
            this._itemCreatedByListBox.Size = new System.Drawing.Size(380, 24);
            this._itemCreatedByListBox.TabIndex = 10;
            // 
            // _createdByLabel
            // 
            this._createdByLabel.AutoSize = true;
            this._createdByLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._createdByLabel.Location = new System.Drawing.Point(30, 410);
            this._createdByLabel.Name = "_createdByLabel";
            this._createdByLabel.Size = new System.Drawing.Size(313, 32);
            this._createdByLabel.TabIndex = 9;
            this._createdByLabel.Text = "Created By Recipe(s):";
            // 
            // _itemUsedByListBox
            // 
            this._itemUsedByListBox.FormattingEnabled = true;
            this._itemUsedByListBox.ItemHeight = 20;
            this._itemUsedByListBox.Location = new System.Drawing.Point(36, 619);
            this._itemUsedByListBox.MaximumSize = new System.Drawing.Size(380, 484);
            this._itemUsedByListBox.MinimumSize = new System.Drawing.Size(380, 26);
            this._itemUsedByListBox.Name = "_itemUsedByListBox";
            this._itemUsedByListBox.Size = new System.Drawing.Size(380, 24);
            this._itemUsedByListBox.TabIndex = 8;
            // 
            // _usedByLabel
            // 
            this._usedByLabel.AutoSize = true;
            this._usedByLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._usedByLabel.Location = new System.Drawing.Point(30, 584);
            this._usedByLabel.Name = "_usedByLabel";
            this._usedByLabel.Size = new System.Drawing.Size(265, 32);
            this._usedByLabel.TabIndex = 7;
            this._usedByLabel.Text = "Used In Recipe(s):";
            // 
            // _itemFriendlyNameTextBox
            // 
            this._itemFriendlyNameTextBox.Location = new System.Drawing.Point(219, 108);
            this._itemFriendlyNameTextBox.MaximumSize = new System.Drawing.Size(200, 4);
            this._itemFriendlyNameTextBox.Name = "_itemFriendlyNameTextBox";
            this._itemFriendlyNameTextBox.ReadOnly = true;
            this._itemFriendlyNameTextBox.Size = new System.Drawing.Size(200, 26);
            this._itemFriendlyNameTextBox.TabIndex = 5;
            // 
            // _itemNameTextBox
            // 
            this._itemNameTextBox.Location = new System.Drawing.Point(219, 68);
            this._itemNameTextBox.Name = "_itemNameTextBox";
            this._itemNameTextBox.ReadOnly = true;
            this._itemNameTextBox.Size = new System.Drawing.Size(200, 26);
            this._itemNameTextBox.TabIndex = 4;
            // 
            // _friendlyNameLabel
            // 
            this._friendlyNameLabel.AutoSize = true;
            this._friendlyNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._friendlyNameLabel.Location = new System.Drawing.Point(22, 105);
            this._friendlyNameLabel.Name = "_friendlyNameLabel";
            this._friendlyNameLabel.Size = new System.Drawing.Size(191, 29);
            this._friendlyNameLabel.TabIndex = 3;
            this._friendlyNameLabel.Text = "Friendly Name:";
            // 
            // _nameLabel
            // 
            this._nameLabel.AutoSize = true;
            this._nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._nameLabel.Location = new System.Drawing.Point(124, 65);
            this._nameLabel.Name = "_nameLabel";
            this._nameLabel.Size = new System.Drawing.Size(89, 29);
            this._nameLabel.TabIndex = 2;
            this._nameLabel.Text = "Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 192);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "Icon";
            // 
            // _itemIconPictureBox
            // 
            this._itemIconPictureBox.Location = new System.Drawing.Point(108, 153);
            this._itemIconPictureBox.Name = "_itemIconPictureBox";
            this._itemIconPictureBox.Size = new System.Drawing.Size(128, 128);
            this._itemIconPictureBox.TabIndex = 0;
            this._itemIconPictureBox.TabStop = false;
            // 
            // _tabControl
            // 
            this._tabControl.Controls.Add(this.tabPage1);
            this._tabControl.Controls.Add(this.tabPage2);
            this._tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tabControl.Location = new System.Drawing.Point(0, 0);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(1737, 1101);
            this._tabControl.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.splitContainer1);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1729, 1068);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Items";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer2);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1729, 1068);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Recipes";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this._recipeFlow);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer2.Panel2.Controls.Add(this._resultsDataGridView);
            this.splitContainer2.Panel2.Controls.Add(this._ingredientDataGridView);
            this.splitContainer2.Panel2.Controls.Add(this.label2);
            this.splitContainer2.Panel2.Controls.Add(this.label3);
            this.splitContainer2.Panel2.Controls.Add(this._recipeFriendlyNameTextBox);
            this.splitContainer2.Panel2.Controls.Add(this._recipeNameTextBox);
            this.splitContainer2.Panel2.Controls.Add(this.label4);
            this.splitContainer2.Panel2.Controls.Add(this.label5);
            this.splitContainer2.Panel2.Controls.Add(this.label6);
            this.splitContainer2.Panel2.Controls.Add(this._recipeIconPictureBox);
            this.splitContainer2.Panel2MinSize = 300;
            this.splitContainer2.Size = new System.Drawing.Size(1723, 1062);
            this.splitContainer2.SplitterDistance = 1211;
            this.splitContainer2.TabIndex = 0;
            // 
            // _recipeFlow
            // 
            this._recipeFlow.AutoScroll = true;
            this._recipeFlow.Dock = System.Windows.Forms.DockStyle.Fill;
            this._recipeFlow.Location = new System.Drawing.Point(0, 0);
            this._recipeFlow.Name = "_recipeFlow";
            this._recipeFlow.Size = new System.Drawing.Size(1211, 1062);
            this._recipeFlow.TabIndex = 0;
            // 
            // _resultsDataGridView
            // 
            this._resultsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._resultsDataGridView.Location = new System.Drawing.Point(44, 438);
            this._resultsDataGridView.Name = "_resultsDataGridView";
            this._resultsDataGridView.RowTemplate.Height = 28;
            this._resultsDataGridView.Size = new System.Drawing.Size(383, 150);
            this._resultsDataGridView.TabIndex = 21;
            // 
            // _ingredientDataGridView
            // 
            this._ingredientDataGridView.Location = new System.Drawing.Point(44, 637);
            this._ingredientDataGridView.Name = "_ingredientDataGridView";
            this._ingredientDataGridView.RowHeadersWidth = 4;
            this._ingredientDataGridView.RowTemplate.Height = 28;
            this._ingredientDataGridView.Size = new System.Drawing.Size(383, 378);
            this._ingredientDataGridView.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(38, 602);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(176, 32);
            this.label2.TabIndex = 19;
            this.label2.Text = "Ingredients:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(38, 402);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 32);
            this.label3.TabIndex = 17;
            this.label3.Text = "Results:";
            // 
            // _recipeFriendlyNameTextBox
            // 
            this._recipeFriendlyNameTextBox.Location = new System.Drawing.Point(227, 75);
            this._recipeFriendlyNameTextBox.MaximumSize = new System.Drawing.Size(200, 4);
            this._recipeFriendlyNameTextBox.Name = "_recipeFriendlyNameTextBox";
            this._recipeFriendlyNameTextBox.ReadOnly = true;
            this._recipeFriendlyNameTextBox.Size = new System.Drawing.Size(200, 26);
            this._recipeFriendlyNameTextBox.TabIndex = 16;
            // 
            // _recipeNameTextBox
            // 
            this._recipeNameTextBox.Location = new System.Drawing.Point(227, 35);
            this._recipeNameTextBox.Name = "_recipeNameTextBox";
            this._recipeNameTextBox.ReadOnly = true;
            this._recipeNameTextBox.Size = new System.Drawing.Size(200, 26);
            this._recipeNameTextBox.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(30, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(191, 29);
            this.label4.TabIndex = 14;
            this.label4.Text = "Friendly Name:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(132, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 29);
            this.label5.TabIndex = 13;
            this.label5.Text = "Name:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(38, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 32);
            this.label6.TabIndex = 12;
            this.label6.Text = "Icon";
            // 
            // _recipeIconPictureBox
            // 
            this._recipeIconPictureBox.Location = new System.Drawing.Point(116, 121);
            this._recipeIconPictureBox.Name = "_recipeIconPictureBox";
            this._recipeIconPictureBox.Size = new System.Drawing.Size(245, 235);
            this._recipeIconPictureBox.TabIndex = 11;
            this._recipeIconPictureBox.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1737, 1101);
            this.Controls.Add(this._tabControl);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._itemIconPictureBox)).EndInit();
            this._tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._resultsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._ingredientDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._recipeIconPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.FlowLayoutPanel _flow;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox _itemIconPictureBox;
        private System.Windows.Forms.TextBox _itemFriendlyNameTextBox;
        private System.Windows.Forms.TextBox _itemNameTextBox;
        private System.Windows.Forms.Label _friendlyNameLabel;
        private System.Windows.Forms.Label _nameLabel;
        private System.Windows.Forms.Label _usedByLabel;
        private System.Windows.Forms.ListBox _itemUsedByListBox;
        private System.Windows.Forms.ListBox _itemCreatedByListBox;
        private System.Windows.Forms.Label _createdByLabel;
        private System.Windows.Forms.TabControl _tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.FlowLayoutPanel _recipeFlow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox _recipeFriendlyNameTextBox;
        private System.Windows.Forms.TextBox _recipeNameTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox _recipeIconPictureBox;
        private System.Windows.Forms.DataGridView _ingredientDataGridView;
        private System.Windows.Forms.DataGridView _resultsDataGridView;
    }
}

